import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;

var worktableArray = [<minecraft:log>,<minecraft:log:1>,<minecraft:log:2>,<minecraft:log:3>,<minecraft:log2>,<minecraft:log2:1>,<primal:logs>,<primal:logs:1>,<primal:planks:2>,<primal:planks:3>] as IItemStack[];
var worktableshelfDef = <primal:worktable_shelf>.definition;

var immPlateArray = [<immersiveengineering:metal>,<immersiveengineering:metal:1>,<immersiveengineering:metal:2>,<immersiveengineering:metal:3>,<immersiveengineering:metal:4>,<immersiveengineering:metal:5>,<immersiveengineering:metal:6>,<immersiveengineering:metal:7>,<immersiveengineering:metal:8>,<minecraft:iron_ingot>,<minecraft:gold_ingot>] as IItemStack[];
var immPlateDef = <immersiveengineering:metal>.definition;

/* var othersPlatesArray as string[IItemStack] = {
<primal:tin_plate> : "<ore:ingotTin>",
<primal:bronze_plate> : "<ore:ingotBronze>",
<primal:brass_plate> : "<ore:ingotBrass>",
<primal:zinc_plate> : "<ore:ingotZinc>",
<contenttweaker:material_part:68> : "<ore:ingotPalladium>",
<contenttweaker:material_part:74> : "<ore:ingotPlatinum>",
<contenttweaker:material_part:83> : "<ore:ingotOsmium>",
<contenttweaker:material_part:89> : "<ore:ingotRedstoneAlloy>",
<contenttweaker:material_part:98> : "<ore:ingotInvar>"
};
*/


recipes.remove(<primal:worktable_shelf:*>);

for meta, item in worktableArray {
	recipes.addShaped("primal_worktable_shelf_" + meta, worktableshelfDef.makeStack(meta),[[<ore:toolAxe>.transformDamage(),item,item],[null,item,item]]);
}

for meta, item in immPlateArray {
	recipes.remove(immPlateDef.makeStack((meta+30)));
	recipes.addShapedMirrored("plate_" + (meta+30), immPlateDef.makeStack((meta+30)), [[item, <immersiveengineering:tool>],[item]]);
}

/*
for i, item in othersPlatesArray {
	recipes.addShapedMirrored("plate_" + i, [[othersPlatesArray[item], <immersiveengineering:tool>],[othersPlatesArray[item]]]);
}*/

recipes.remove(<tconstruct:stone_stick>);

recipes.remove(<minecraft:torch>);
recipes.removeShaped(<minecraft:torch>, [[<minecraft:coal:*>], [<ore:stickWood>]]);
recipes.removeShaped(<minecraft:torch>, [[<betterwithmods:material:1>], [<ore:stickWood>]]);
recipes.removeByRegex("minecraft:torch");
recipes.removeByRegex("betterwithmods:torch");

recipes.removeByRegex("quark:repeater");
recipes.removeByRegex("cyclicmagic:item.diode_1");
recipes.removeByRegex("minecraft:leather");
recipes.removeByRegex("betterwithmods:hcdiamond/diamond_helmet");
recipes.removeByRegex("betterwithmods:hcdiamond/diamond_chestplate");
recipes.removeByRegex("betterwithmods:hcdiamond/diamond_leggings");
recipes.removeByRegex("betterwithmods:hcdiamond/diamond_boots");

furnace.remove(<primal:diamond_plate>);

recipes.remove(<primal:torch_wood>);

recipes.addShaped("BasicTorch_Charcoal", <primal:torch_wood>, [[<ore:clumpCharcoal>,<ore:cordagePlant>], [<ore:stickWood>]]);
recipes.addShaped("BasicTorch_Coal", <primal:torch_wood>*2, [[<ore:clumpCoal>,<ore:cordagePlant>], [<ore:stickWood>]]);
recipes.addShaped("BasicTorch_CoalCoke", <primal:torch_wood>*4, [[<immersiveengineering:material:6>,<ore:cordagePlant>], [<ore:stickWood>]]);
recipes.addShaped("immersiveengineering_material_torch", <primal:torch_wood> * 12, [[<minecraft:wool:*>, <forge:bucketfilled>.withTag({FluidName: "creosote", Amount: 1000}), null], [<ore:stickWood>, <ore:stickWood>, <ore:stickWood>]]);

furnace.remove(<rustic:tallow>);
mods.jei.JEI.hide(<rustic:tallow>);


recipes.remove(<betterwithaddons:aqueduct:*>);
recipes.addShaped("aqueduct_stonebrick", <betterwithaddons:aqueduct:0> * 3, [[<minecraft:clay>, <minecraft:clay>, <minecraft:clay>], [<minecraft:stonebrick>, <minecraft:stonebrick>, <minecraft:stonebrick>]]);
recipes.addShaped("aqueduct_brick_block", <betterwithaddons:aqueduct:1> * 3, [[<minecraft:clay>, <minecraft:clay>, <minecraft:clay>], [<minecraft:brick_block>, <minecraft:brick_block>, <minecraft:brick_block>]]);
recipes.addShaped("aqueduct_quartz_block", <betterwithaddons:aqueduct:2> * 3, [[<minecraft:clay>, <minecraft:clay>, <minecraft:clay>], [<minecraft:quartz_block:*>, <minecraft:quartz_block:*>, <minecraft:quartz_block:*>]]);
recipes.addShaped("aqueduct_whitebrick", <betterwithaddons:aqueduct:3> * 3, [[<minecraft:clay>, <minecraft:clay>, <minecraft:clay>], [<betterwithaddons:whitebrick>, <betterwithaddons:whitebrick>, <betterwithaddons:whitebrick>]]);
recipes.addShaped("aqueduct_sandstone", <betterwithaddons:aqueduct:4> * 3, [[<minecraft:clay>, <minecraft:clay>, <minecraft:clay>], [<minecraft:sandstone:*>, <minecraft:sandstone:*>, <minecraft:sandstone:*>]]);
recipes.addShaped("aqueduct_red_sandstone", <betterwithaddons:aqueduct:5> * 3, [[<minecraft:clay>, <minecraft:clay>, <minecraft:clay>], [<minecraft:red_sandstone:*>, <minecraft:red_sandstone:*>, <minecraft:red_sandstone:*>]]);
recipes.addShaped("aqueduct_andesite", <betterwithaddons:aqueduct:6> * 3, [[<minecraft:clay>, <minecraft:clay>, <minecraft:clay>], [<minecraft:stone:6>, <minecraft:stone:6>, <minecraft:stone:6>]]);
recipes.addShaped("aqueduct_granite", <betterwithaddons:aqueduct:7> * 3, [[<minecraft:clay>, <minecraft:clay>, <minecraft:clay>], [<minecraft:stone:2>, <minecraft:stone:2>, <minecraft:stone:2>]]);
recipes.addShaped("aqueduct_diorite", <betterwithaddons:aqueduct:8> * 3, [[<minecraft:clay>, <minecraft:clay>, <minecraft:clay>], [<minecraft:stone:4>, <minecraft:stone:4>, <minecraft:stone:4>]]);
recipes.addShaped("aqueduct_prismarinebrick", <betterwithaddons:aqueduct:9> * 3, [[<minecraft:clay>, <minecraft:clay>, <minecraft:clay>], [<minecraft:prismarine:1>, <minecraft:prismarine:1>, <minecraft:prismarine:1>]]);
recipes.addShaped("aqueduct_darkprismarine", <betterwithaddons:aqueduct:10> * 3, [[<minecraft:clay>, <minecraft:clay>, <minecraft:clay>], [<minecraft:prismarine:2>, <minecraft:prismarine:2>, <minecraft:prismarine:2>]]);

recipes.remove(<betterwithmods:material:45>);
recipes.addShapeless("bwm_diamondIngot",<betterwithmods:material:45>, [<minecraft:iron_ingot>, <minecraft:diamond>, <betterwithmods:creeper_oyster>]);

recipes.remove(<minecraft:bucket>);
recipes.addShaped("minecraft_bucket_fix",<minecraft:bucket>,[[<ore:plateIron>, null, <ore:plateIron>],[null, <ore:plateIron>, null]]);

//Iron Chest need to have good progression, because if not, this is is just a freacking OP!!
recipes.remove(<ironchest:iron_chest:3>);
recipes.addShaped("chest_copper",<ironchest:iron_chest:3>,
	[[<immersiveengineering:metal:30>,<immersiveengineering:metal:30>,<immersiveengineering:metal:30>],
	 [<immersiveengineering:metal:30>,<ore:chestWood>,<immersiveengineering:metal:30>],
	 [<immersiveengineering:metal:30>,<immersiveengineering:metal:30>,<immersiveengineering:metal:30>]]);
recipes.remove(<ironchest:iron_chest>);
recipes.addShaped("chest_iron",<ironchest:iron_chest>,
	[[<immersiveengineering:metal:39>,<immersiveengineering:metal:39>,<immersiveengineering:metal:39>],
	 [<immersiveengineering:metal:39>,<ironchest:iron_chest:3>,<immersiveengineering:metal:39>],
	 [<immersiveengineering:metal:39>,<immersiveengineering:metal:39>,<immersiveengineering:metal:39>]]);
recipes.remove(<ironchest:iron_chest:4>);
recipes.addShaped("chest_silver",<ironchest:iron_chest:4>,
	[[<immersiveengineering:metal:33>,<immersiveengineering:metal:33>,<immersiveengineering:metal:33>],
	 [<immersiveengineering:metal:33>,<ironchest:iron_chest>,<immersiveengineering:metal:33>],
	 [<immersiveengineering:metal:33>,<immersiveengineering:metal:33>,<immersiveengineering:metal:33>]]);
recipes.remove(<ironchest:iron_chest:1>);
recipes.addShaped("chest_gold",<ironchest:iron_chest:1>,
	[[<immersiveengineering:metal:40>,<immersiveengineering:metal:40>,<immersiveengineering:metal:40>],
	 [<immersiveengineering:metal:40>,<ironchest:iron_chest:4>,<immersiveengineering:metal:40>],
	 [<immersiveengineering:metal:40>,<immersiveengineering:metal:40>,<immersiveengineering:metal:40>]]);
recipes.remove(<ironchest:iron_chest:2>);
recipes.addShaped("chest_diamound",<ironchest:iron_chest:2>,
	[[<primal:diamond_plate>,<primal:diamond_plate>,<primal:diamond_plate>],
	 [<primal:diamond_plate>,<ironchest:iron_chest:1>,<primal:diamond_plate>],
	 [<primal:diamond_plate>,<primal:diamond_plate>,<primal:diamond_plate>]]);
recipes.remove(<ironchest:iron_chest:6>);
recipes.addShaped("chest_obsidian",<ironchest:iron_chest:6>,
	[[<primal:obsidian_plate>,<primal:obsidian_plate>,<primal:obsidian_plate>],
	 [<primal:obsidian_plate>,<ironchest:iron_chest:1>,<primal:obsidian_plate>],
	 [<primal:obsidian_plate>,<primal:obsidian_plate>,<primal:obsidian_plate>]]);


var water_bottle = <minecraft:potion>.withTag({Potion: "minecraft:water"});

recipes.remove(<minecraft:compass>);

recipes.addShapedMirrored("realistic_compass",<minecraft:compass>,
	[[<minecraft:glass_pane>,<immersiveengineering:metal:39>, water_bottle.transformReplace(<minecraft:glass_bottle>)],
	 [<immersiveengineering:metal:39>,<immersiveengineering:material:3>,<immersiveengineering:metal:39>],
	 [<minecraft:redstone>,<immersiveengineering:metal:39>,<minecraft:redstone>]]);

recipes.remove(<betterwithmods:bellows>);
recipes.addShaped("betterwithmods_bellows", <betterwithmods:bellows>, [[sidingPlankWood, sidingPlankWood, sidingPlankWood], [<ore:leatherTanned>, <ore:leatherTanned>, <ore:leatherTanned>], [<ore:hideBelt>, <ore:gearWood>, <ore:hideBelt>]]);


recipes.removeByRegex("minecraft:clock");
recipes.addShapedMirrored("realistic_clock", <minecraft:clock>,
	[[<primal:tin_plate>,<ore:ingotBronze>,<ore:stickIron>],
	 [<ore:ingotBronze>,<minecraft:quartz>,<ore:ingotBronze>],
	 [<ore:stickIron>,<ore:ingotBronze>,<primal:tin_plate>]]);

recipes.addShaped("fix_leather_boots", <minecraft:leather_boots>,
	[[<ore:cordageQuality>,null,<ore:cordageQuality>],
	 [<ore:leather>,null,<ore:leather>],
	 [<ore:leather>,null,<ore:leather>]]);

recipes.remove(<minecraft:netherbrick>);
furnace.remove(<minecraft:netherbrick>);

recipes.remove(<minecraft:saddle>);
recipes.addShaped("fix_saddle",<minecraft:saddle>,
	[[<betterwithmods:material:6>,<betterwithmods:material:6>,<betterwithmods:material:6>],
	 [<betterwithmods:material:8>,<betterwithmods:material:6>,<betterwithmods:material:8>],
	 [<primal:iron_ring>,null,<primal:iron_ring>]]);

var dustMetal as IItemStack[] = [<immersiveengineering:metal:9>,<immersiveengineering:metal:10>,<immersiveengineering:metal:11>,<immersiveengineering:metal:12>,<immersiveengineering:metal:13>,<immersiveengineering:metal:14>,<immersiveengineering:metal:15>,<immersiveengineering:metal:16>,<immersiveengineering:metal:17>,<immersiveengineering:metal:18>,<immersiveengineering:metal:19>];

for dust in dustMetal {
	recipes.remove(dust);
}

recipes.remove(<minecraft:iron_bars>);
recipes.addShaped("fix_ironBars",<minecraft:iron_bars>*3,
	[[<ore:stickIron>,<ore:stickIron>,<ore:stickIron>],
	[<ore:stickIron>,<ore:stickIron>,<ore:stickIron>]]);


recipes.removeByRegex("uppers:upper");
recipes.addShaped("fix_hc_upper", <minecraft:hopper>,
	[[null,<ore:plateIron>,null],
	 [<ore:plateIron>,latchRedstone,<ore:plateIron>],
	 [<ore:plateIron>,<ore:chestWood>,<ore:plateIron>]]);

recipes.removeByRegex("minecraft:hopper");
recipes.addShaped("fix_hc_hopper", <minecraft:hopper>,
	[[<ore:plateIron>,<ore:chestWood>,<ore:plateIron>],
	 [<ore:plateIron>,latchRedstone,<ore:plateIron>],
	 [null,<ore:plateIron>,null]]);

recipes.removeByRegex("quark:dispenser");
recipes.removeByRegex("cyclicmagic:tile.dispenser_1");

recipes.addShaped("fix_charcoal_block",<quark:charcoal_block>,
	[[<ore:clumpCharcoal>,<ore:clumpCharcoal>,<ore:clumpCharcoal>],
	 [<ore:clumpCharcoal>,<ore:clumpCharcoal>,<ore:clumpCharcoal>],
	 [<ore:clumpCharcoal>,<ore:clumpCharcoal>,<ore:clumpCharcoal>]]);
