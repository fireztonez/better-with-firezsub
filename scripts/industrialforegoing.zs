#priority 700
import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import crafttweaker.oredict.IOreDictEntry;
import mods.betterwithmods.MiniBlocks;

recipes.remove(<industrialforegoing:range_addon>);
recipes.addShaped("range_addon_1",<industrialforegoing:range_addon>,
	[[sidingStone, plastic, sidingStone],
	 [sidingStone, <ore:paneGlass>, sidingStone],
	 [sidingStone, plastic, sidingStone]]);

var rangeAddonsDef = <industrialforegoing:range_addon>.definition;
var rangeAddonItems = [<ore:dyeBlue>,<ore:ingotTin>,<ore:ingotIron>,<ore:ingotCopper>,<ore:ingotBronze>,<ore:ingotSilver>,<ore:ingotGold>,<ore:gemQuartz>,<ore:gemDiamond>,<ore:ingotPlatinum>,<ore:gemEmerald>] as IIngredient[];
var item = 0;

for meta in 1 to 12 {
	item = meta - 1;
	recipes.remove(rangeAddonsDef.makeStack(meta));
	recipes.addShaped("range_addon_" + (meta + 1), rangeAddonsDef.makeStack(meta),
	[[rangeAddonItems[item], plastic, rangeAddonItems[item]],
	 [rangeAddonItems[item], rangeAddonsDef.makeStack(item), rangeAddonItems[item]],
	 [rangeAddonItems[item], plastic, rangeAddonItems[item]]]);
}
