
//brewing.removeRecipe(IItemStack input, IItemStack ingredient);
brewing.removeRecipe(<minecraft:potion>.withTag({Potion: "cyclicmagic:bounce"}), <minecraft:slime_ball>); #Remove for using Slime ball for Jump Boost Potion
brewing.removeRecipe(<minecraft:potion>.withTag({Potion: "cofhcore:luck"}), <minecraft:emerald>);
brewing.removeRecipe(<minecraft:potion>.withTag({Potion: "cyclicmagic:luck"}), <minecraft:dye:2>);
brewing.removeRecipe(<minecraft:potion>.withTag({Potion: "quark:haste"}), <minecraft:prismarine_crystals>);
brewing.removeRecipe(<minecraft:potion>.withTag({Potion: "cyclicmagic:haste"}), <minecraft:cookie>);
brewing.removeRecipe(<minecraft:potion>.withTag({Potion: "quark:resistance"}), <quark:biotite>);
brewing.removeRecipe(<minecraft:potion>.withTag({Potion: "cyclicmagic:resistance"}), <minecraft:obsidian>);
brewing.removeRecipe(<minecraft:potion>.withTag({Potion: "cyclicmagic:levitation"}), <minecraft:chorus_fruit>);
brewing.removeRecipe(<minecraft:potion>.withTag({Potion: "cofhcore:levitation"}), <thermalfoundation:material:2051>);

//A Brewing Recipe consists of 3 Parts:
//
//An Input (What is in the 3 “Bottle Slots”)
//One or more possible Ingredients (What can go into the upper slot where netherwart goes)
//The output (What the recipe returns)
//example:
//brewing.addBrew(IIngredient input, IIngredient ingredient, IItemStack output, @Optional boolean hidden);
//brewing.addBrew(<ore:blockGlass>, <ore:logWood>, <minecraft:beacon>);
//brewing.addBrew(<ore:ingotGold>, <minecraft:obsidian>, <minecraft:wool:3>, true);
//
//brewing.addBrew(IIngredient input, IIngredient[] ingredients, IItemStack output, @Optional boolean hidden);
//brewing.addBrew(<minecraft:bedrock>, [<minecraft:lapis_ore>], <minecraft:sponge:1>);
//brewing.addBrew(<minecraft:gold_block>, [<minecraft:iron_block>, <minecraft:lapis_block>], <minecraft:sponge:1>, true);
brewing.addBrew(<minecraft:potion>.withTag({Potion: "minecraft:awkward"}), <minecraft:slime_ball>, <minecraft:potion>.withTag({Potion: "minecraft:leaping"})); #Vanilla Jump Boost Potion need recipe

brewing.addBrew(<minecraft:potion>.withTag({Potion: "minecraft:awkward"}), sugar, <minecraft:potion>.withTag({Potion: "minecraft:swiftness"})); #Vanilla Speed Potion need recipe
brewing.addBrew(<minecraft:potion>.withTag({Potion: "minecraft:swiftness"}), witchWart, <minecraft:potion>.withTag({Potion: "minecraft:long_swiftness"})); #Vanilla Speed Potion need recipe
brewing.addBrew(<minecraft:potion>.withTag({Potion: "minecraft:swiftness"}), sulfur, <minecraft:potion>.withTag({Potion: "minecraft:strong_swiftness"})); #Vanilla Speed Potion need recipe

brewing.addBrew(<minecraft:potion>.withTag({Potion: "minecraft:awkward"}), <ore:blockSlime>, <minecraft:potion>.withTag({Potion: "cyclicmagic:bounce"})); #Cyclic Bouncy Potion with Slime Block need recipe

brewing.addBrew(<minecraft:potion>.withTag({Potion: "minecraft:awkward"}), <minecraft:rabbit_foot>, <minecraft:potion>.withTag({Potion: "cofhcore:luck"}));

brewing.addBrew(<minecraft:potion>.withTag({Potion: "minecraft:awkward"}), <darkutils:shulker_pearl>, <minecraft:potion>.withTag({Potion: "cofhcore:levitation"}));

brewing.addBrew(<minecraft:potion>.withTag({Potion: "minecraft:awkward"}), <quark:biotite>, <minecraft:potion>.withTag({Potion: "cofhcore:resistance"}));
