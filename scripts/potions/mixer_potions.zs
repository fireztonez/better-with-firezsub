import mods.immersiveengineering.Mixer;
import mods.immersiveengineering.BottlingMachine;

//--------------
//Remove old incorrect potions recipes
//--------------
//Examples:
//mods.immersiveengineering.Mixer.removeRecipe(ILiquidStack output);
//mods.immersiveengineering.Mixer.removeRecipe(<liquid:lava>);

Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "minecraft:night_vision"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "minecraft:long_night_vision"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "minecraft:invisibility"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "minecraft:long_invisibility"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "minecraft:leaping"}) * 1000); // Jump Boost Potion Fix
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "minecraft:long_leaping"}) * 1000); // Long Jump Boost Potion Fix
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "minecraft:strong_leaping"}) * 1000); // Long Jump Boost Potion Fix
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "minecraft:long_fire_resistance"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "minecraft:strong_swiftness"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "minecraft:long_swiftness"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "minecraft:slowness"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "minecraft:long_slowness"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "minecraft:water_breathing"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "minecraft:long_water_breathing"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "minecraft:healing"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "minecraft:strong_healing"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "minecraft:poison"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "minecraft:long_poison"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "minecraft:strong_poison"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "minecraft:harming"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "minecraft:strong_harming"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "minecraft:long_regeneration"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "minecraft:strong_regeneration"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "minecraft:long_strength"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "minecraft:strong_strength"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "minecraft:weakness"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "minecraft:long_weakness"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "quark:haste"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "cyclicmagic:haste"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "quark:resistance"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "cyclicmagic:resistance"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "cyclicmagic:levitation"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "cofhcore:levitation"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "cofhcore:luck"}) * 1000);
Mixer.removeRecipe(<liquid:potion>.withTag({Potion: "cyclicmagic:luck"}) * 1000);


//--------------
//Add Potions recipes
//--------------
//Examples:
//Example:
//mods.immersiveengineering.Mixer.addRecipe(ILiquidStack output, ILiquidStack fluidInput, IIngredient[] itemInputs, int energy);
//mods.immersiveengineering.Mixer.addRecipe(<liquid:lava>, <liquid:water>, [<ore:logWood>, <minecraft:dirt>], 2048);

Mixer.addRecipe(<liquid:potion>.withTag({Potion: "minecraft:night_vision"}) * 1000,
    <liquid:potion>.withTag({Potion: "minecraft:awkward"}) * 1000,
        [spiderEye],
        3000);

Mixer.addRecipe(<liquid:potion>.withTag({Potion: "minecraft:long_night_vision"}) * 1000,
    <liquid:potion>.withTag({Potion: "minecraft:night_vision"}) * 1000,
        [witchWart],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:invisibility"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:night_vision"}) * 1000,
        [poisonSac],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:long_invisibility"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:invisibility"}) * 1000,
        [poisonSac],
        3000);
Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:long_invisibility"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:invisibility"}) * 1000,
        [witchWart],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:leaping"}) * 1000, //Jump Boost
        <liquid:potion>.withTag({Potion: "minecraft:awkward"}) * 1000,
        [<minecraft:slime_ball>],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:long_leaping"}) * 1000, //Long Jump Boost
        <liquid:potion>.withTag({Potion: "minecraft:leaping"}) * 1000,
        [witchWart],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:strong_leaping"}) * 1000, //Long Jump Boost
        <liquid:potion>.withTag({Potion: "minecraft:leaping"}) * 1000,
        [sulfur],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:long_leaping"}) * 1000, //Long Jump Boost
        <liquid:potion>.withTag({Potion: "minecraft:leaping"}) * 1000,
        [witchWart],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:long_fire_resistance"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:fire_resistance"}) * 1000,
        [witchWart],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:strong_swiftness"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:swiftness"}) * 1000,
        [sulfur],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:long_swiftness"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:swiftness"}) * 1000,
        [witchWart],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:slowness"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:swiftness"}) * 1000,
        [poisonSac],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:long_slowness"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:slowness"}) * 1000,
        [witchWart],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:healing"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:awkward"}) * 1000,
        [mysteriousGland],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:strong_healing"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:healing"}) * 1000,
        [sulfur],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:water_breathing"}) * 1000,
    <liquid:potion>.withTag({Potion: "minecraft:awkward"}) * 1000,
        [<ore:listAllfishraw>], 3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:long_water_breathing"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:water_breathing"}) * 1000,
        [witchWart],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:harming"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:healing"}) * 1000,
        [poisonSac],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:harming"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:long_poison"}) * 1000,
        [poisonSac],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:harming"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:poison"}) * 1000,
        [poisonSac],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:strong_harming"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:strong_poison"}) * 1000,
        [poisonSac],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:strong_harming"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:strong_healing"}) * 1000,
        [poisonSac],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:strong_harming"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:harming"}) * 1000,
        [sulfur],
        3000);


Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:poison"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:awkward"}) * 1000,
        [<minecraft:red_mushroom>],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:long_poison"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:poison"}) * 1000,
        [witchWart],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:strong_poison"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:poison"}) * 1000,
        [sulfur],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:long_regeneration"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:regeneration"}) * 1000,
        [witchWart],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:strong_regeneration"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:regeneration"}) * 1000,
        [sulfur],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:strong_regeneration"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:regeneration"}) * 1000,
        [sulfur],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:long_strength"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:strength"}) * 1000,
        [witchWart],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:strong_strength"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:strength"}) * 1000,
        [sulfur],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:weakness"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:strength"}) * 1000,
        [poisonSac],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:weakness"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:strong_strength"}) * 1000,
        [poisonSac],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:long_weakness"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:long_strength"}) * 1000,
        [poisonSac],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "minecraft:long_weakness"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:weakness"}) * 1000,
        [witchWart],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "cofhcore:levitation"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:awkward"}) * 1000,
        [<darkutils:shulker_pearl>],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "cofhcore:levitation"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:awkward"}) * 1000,
        [<darkutils:shulker_pearl>],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "cofhcore:resistance"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:awkward"}) * 1000,
        [<quark:biotite>],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "cofhcore:luck"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:awkward"}) * 1000,
        [<minecraft:rabbit_foot>],
        3000);

Mixer.addRecipe(
    <liquid:potion>.withTag({Potion: "cofhcore:levitation"}) * 1000,
        <liquid:potion>.withTag({Potion: "minecraft:awkward"}) * 1000,
        [<darkutils:shulker_pearl>],
        3000);


//----------------
//adding new Potions Bottling to the Bottling Machine!
//----------------
//mods.immersiveengineering.BottlingMachine.addRecipe(IItemStack output, IIngredient input, ILiquidStack fluid);
//mods.immersiveengineering.BottlingMachine.addRecipe(<minecraft:diamond>, <ore:logWood>, <liquid:water>);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:night_vision"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:night_vision"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:long_night_vision"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:long_night_vision"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:invisibility"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:invisibility"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:long_invisibility"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:long_invisibility"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:leaping"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:leaping"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:long_leaping"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:long_leaping"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:strong_leaping"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:strong_leaping"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:long_fire_resistance"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:long_fire_resistance"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:strong_swiftness"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:strong_swiftness"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:long_swiftness"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:long_swiftness"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:slowness"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:slowness"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:long_slowness"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:long_slowness"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:water_breathing"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:water_breathing"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:long_water_breathing"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:long_water_breathing"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:healing"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:healing"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:strong_healing"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:strong_healing"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:strong_healing"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:strong_healing"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:poison"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:poison"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:long_poison"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:long_poison"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:strong_poison"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:strong_poison"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:night_vision"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:night_vision"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:invisibility"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:invisibility"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:long_invisibility"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:long_invisibility"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:long_fire_resistance"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:long_fire_resistance"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:water_breathing"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:water_breathing"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:long_water_breathing"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:long_water_breathing"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:harming"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:harming"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:strong_harming"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:strong_harming"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:long_regeneration"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:long_regeneration"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:strong_regeneration"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:strong_regeneration"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:long_strength"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:long_strength"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:weakness"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:weakness"}) * 250);
BottlingMachine.addRecipe(<minecraft:potion>.withTag({Potion: "minecraft:long_weakness"}), <minecraft:glass_bottle>, <liquid:potion>.withTag({Potion: "minecraft:long_weakness"}) * 250);


BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:night_vision"}), <minecraft:potion>.withTag({Potion: "minecraft:night_vision"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:long_night_vision"}), <minecraft:potion>.withTag({Potion: "minecraft:long_night_vision"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:invisibility"}), <minecraft:potion>.withTag({Potion: "minecraft:invisibility"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:long_invisibility"}), <minecraft:potion>.withTag({Potion: "minecraft:long_invisibility"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:leaping"}), <minecraft:potion>.withTag({Potion: "minecraft:leaping"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:long_leaping"}), <minecraft:potion>.withTag({Potion: "minecraft:long_leaping"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:strong_leaping"}), <minecraft:potion>.withTag({Potion: "minecraft:strong_leaping"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:fire_resistance"}), <minecraft:potion>.withTag({Potion: "minecraft:fire_resistance"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:long_fire_resistance"}), <minecraft:potion>.withTag({Potion: "minecraft:long_fire_resistance"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:swiftness"}), <minecraft:potion>.withTag({Potion: "minecraft:swiftness"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:long_swiftness"}), <minecraft:potion>.withTag({Potion: "minecraft:long_swiftness"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:strong_swiftness"}), <minecraft:potion>.withTag({Potion: "minecraft:strong_swiftness"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:slowness"}), <minecraft:potion>.withTag({Potion: "minecraft:slowness"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:long_slowness"}), <minecraft:potion>.withTag({Potion: "minecraft:long_slowness"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:water_breathing"}), <minecraft:potion>.withTag({Potion: "minecraft:water_breathing"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:long_water_breathing"}), <minecraft:potion>.withTag({Potion: "minecraft:long_water_breathing"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:healing"}), <minecraft:potion>.withTag({Potion: "minecraft:healing"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:strong_healing"}), <minecraft:potion>.withTag({Potion: "minecraft:strong_healing"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:harming"}), <minecraft:potion>.withTag({Potion: "minecraft:harming"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:strong_harming"}), <minecraft:potion>.withTag({Potion: "minecraft:strong_harming"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:poison"}), <minecraft:potion>.withTag({Potion: "minecraft:poison"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:long_poison"}), <minecraft:potion>.withTag({Potion: "minecraft:long_poison"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:strong_poison"}), <minecraft:potion>.withTag({Potion: "minecraft:strong_poison"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:regeneration"}), <minecraft:potion>.withTag({Potion: "minecraft:regeneration"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:long_regeneration"}), <minecraft:potion>.withTag({Potion: "minecraft:long_regeneration"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:strong_regeneration"}), <minecraft:potion>.withTag({Potion: "minecraft:strong_regeneration"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:strength"}), <minecraft:potion>.withTag({Potion: "minecraft:strength"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:long_strength"}), <minecraft:potion>.withTag({Potion: "minecraft:long_strength"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:strong_strength"}), <minecraft:potion>.withTag({Potion: "minecraft:strong_strength"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:weakness"}), <minecraft:potion>.withTag({Potion: "minecraft:weakness"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:long_weakness"}), <minecraft:potion>.withTag({Potion: "minecraft:long_weakness"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "minecraft:luck"}), <minecraft:potion>.withTag({Potion: "minecraft:luck"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:haste"}), <minecraft:potion>.withTag({Potion: "cofhcore:haste"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:haste+"}), <minecraft:potion>.withTag({Potion: "cofhcore:haste+"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:haste2"}), <minecraft:potion>.withTag({Potion: "cofhcore:haste2"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:resistance"}), <minecraft:potion>.withTag({Potion: "cofhcore:resistance"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:resistance+"}), <minecraft:potion>.withTag({Potion: "cofhcore:resistance+"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:resistance2"}), <minecraft:potion>.withTag({Potion: "cofhcore:resistance2"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:levitation"}), <minecraft:potion>.withTag({Potion: "cofhcore:levitation"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:levitation+"}), <minecraft:potion>.withTag({Potion: "cofhcore:levitation+"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:absorption"}), <minecraft:potion>.withTag({Potion: "cofhcore:absorption"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:absorption+"}), <minecraft:potion>.withTag({Potion: "cofhcore:absorption+"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:absorption2"}), <minecraft:potion>.withTag({Potion: "cofhcore:absorption2"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:luck"}), <minecraft:potion>.withTag({Potion: "cofhcore:luck"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:luck+"}), <minecraft:potion>.withTag({Potion: "cofhcore:luck+"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:luck2"}), <minecraft:potion>.withTag({Potion: "cofhcore:luck2"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:unluck"}), <minecraft:potion>.withTag({Potion: "cofhcore:unluck"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:unluck+"}), <minecraft:potion>.withTag({Potion: "cofhcore:unluck+"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:unluck2"}), <minecraft:potion>.withTag({Potion: "cofhcore:unluck2"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:wither"}), <minecraft:potion>.withTag({Potion: "cofhcore:wither"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:wither+"}), <minecraft:potion>.withTag({Potion: "cofhcore:wither+"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:wither2"}), <minecraft:potion>.withTag({Potion: "cofhcore:wither2"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:leaping3"}), <minecraft:potion>.withTag({Potion: "cofhcore:leaping3"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:leaping4"}), <minecraft:potion>.withTag({Potion: "cofhcore:leaping4"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:swiftness3"}), <minecraft:potion>.withTag({Potion: "cofhcore:swiftness3"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:swiftness4"}), <minecraft:potion>.withTag({Potion: "cofhcore:swiftness4"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:healing3"}), <minecraft:potion>.withTag({Potion: "cofhcore:healing3"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:healing4"}), <minecraft:potion>.withTag({Potion: "cofhcore:healing4"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:harming3"}), <minecraft:potion>.withTag({Potion: "cofhcore:harming3"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:harming4"}), <minecraft:potion>.withTag({Potion: "cofhcore:harming4"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:poison3"}), <minecraft:potion>.withTag({Potion: "cofhcore:poison3"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:poison4"}), <minecraft:potion>.withTag({Potion: "cofhcore:poison4"}), <liquid:kerosene> * 25);
BottlingMachine.addRecipe(<minecraft:splash_potion>.withTag({Potion: "cofhcore:regeneration3"}), <minecraft:potion>.withTag({Potion: "cofhcore:regeneration3"}), <liquid:kerosene> * 25);
