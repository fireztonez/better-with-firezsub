recipes.addShaped("astral_luminousCraftingTable",<astralsorcery:blockaltar>,
	[[<ore:stoneMarble>,<astralsorcery:blockblackmarble>,<ore:stoneMarble>],
	 [<ore:stoneMarble>,<minecraft:crafting_table>,<ore:stoneMarble>],
	 [sidingTreatedWood,<astralsorcery:itemcraftingcomponent>,sidingTreatedWood]]);

recipes.addShaped("astral_wand",<astralsorcery:itemwand>,
	[[null,<ore:gemAquamarine>,<minecraft:ender_pearl>],
	 [null,<ore:stoneMarble>,<ore:gemAquamarine>],
	 [<ore:stoneMarble>,null,null]]);
