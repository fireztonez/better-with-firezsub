import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;

var thermalTools as IItemStack[string][string] = {
    copper: {
        excavator: <thermalfoundation:tool.excavator_copper>,
        hammer: <thermalfoundation:tool.hammer_copper>
    },
    tin: {
        excavator: <thermalfoundation:tool.excavator_tin>,
        hammer: <thermalfoundation:tool.hammer_tin>
    },
    silver: {
        excavator: <thermalfoundation:tool.excavator_silver>,
        hammer: <thermalfoundation:tool.hammer_silver>
    },
    lead: {
        excavator: <thermalfoundation:tool.excavator_lead>,
        hammer: <thermalfoundation:tool.hammer_lead>
    },
    aluminum: {
        excavator: <thermalfoundation:tool.excavator_aluminum>,
        hammer: <thermalfoundation:tool.hammer_aluminum>
    },
    nickel: {
        excavator: <thermalfoundation:tool.excavator_nickel>,
        hammer: <thermalfoundation:tool.hammer_nickel>
    },
    platinum: {
        excavator: <thermalfoundation:tool.excavator_platinum>,
        hammer: <thermalfoundation:tool.hammer_platinum>
    },
    steel: {
        excavator: <thermalfoundation:tool.excavator_steel>,
        hammer: <thermalfoundation:tool.hammer_steel>
    },
    electrum: {
        excavator: <thermalfoundation:tool.excavator_electrum>,
        hammer: <thermalfoundation:tool.hammer_electrum>
    },
    invar: {
        excavator: <thermalfoundation:tool.excavator_invar>,
        hammer: <thermalfoundation:tool.hammer_invar>
    },
    bronze: {
        excavator: <thermalfoundation:tool.excavator_bronze>,
        hammer: <thermalfoundation:tool.hammer_bronze>
    },
    constantan: {
        excavator: <thermalfoundation:tool.excavator_constantan>,
        hammer: <thermalfoundation:tool.hammer_constantan>
    },
    iron: {
        excavator: <thermalfoundation:tool.excavator_iron>,
        hammer: <thermalfoundation:tool.hammer_iron>
    },
    diamond: {
        excavator: <thermalfoundation:tool.excavator_diamond>,
        hammer: <thermalfoundation:tool.hammer_diamond>
    },
    gold: {
        excavator: <thermalfoundation:tool.excavator_gold>,
        hammer: <thermalfoundation:tool.hammer_gold>
    }
};

for material in thermalTools {
    recipes.remove(tool.excavator);
}
