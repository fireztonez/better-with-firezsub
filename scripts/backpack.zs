import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import crafttweaker.oredict.IOreDictEntry;
import mods.betterwithmods.MiniBlocks;





recipes.remove(<improvedbackpacks:backpack>);
recipes.addShaped("improvedbackpacks_bag",<improvedbackpacks:backpack>,
	[[<minecraft:string>,<betterwithmods:material:6>,<minecraft:string>],
	 [<betterwithmods:material:6>,<ore:chestWood>,<betterwithmods:material:6>],
	 [<betterwithmods:material:4>,<betterwithmods:material:6>,<betterwithmods:material:4>]]);

recipes.remove(<improvedbackpacks:blank_upgrade>);
recipes.addShaped("improvedbackpacks_blank_upgarde",<improvedbackpacks:blank_upgrade>,
	[[null,<betterwithmods:material:6>,null],
	 [<betterwithmods:material:6>,<ore:plankWood>,<betterwithmods:material:6>],
	 [null,<betterwithmods:material:6>,null]]);
recipes.remove(<improvedbackpacks:upgrade>);
recipes.addShaped("improvedbackpacks_wood_upgarde",<improvedbackpacks:upgrade>,
	[[null,<betterwithmods:material:6>,null],
	 [<ore:logWood>,<improvedbackpacks:blank_upgrade>,<ore:logWood>],
	 [null,<ore:logWood>,null]]);
recipes.remove(<improvedbackpacks:upgrade:1>);
recipes.addShaped("improvedbackpacks_stone_upgarde",<improvedbackpacks:upgrade:1>,
	[[null,<betterwithmods:material:6>,null],
	 [<ore:stone>,<improvedbackpacks:upgrade>,<ore:stone>],
	 [null,<ore:stone>,null]]);
recipes.remove(<improvedbackpacks:upgrade:2>);
recipes.addShaped("improvedbackpacks_iron_upgarde",<improvedbackpacks:upgrade:2>,
	[[null,<betterwithmods:material:6>,null],
	 [<immersiveengineering:metal:39>,<improvedbackpacks:upgrade:1>,<immersiveengineering:metal:39>],
	 [null,<immersiveengineering:metal:39>,null]]);
recipes.remove(<improvedbackpacks:upgrade:3>);
recipes.addShaped("improvedbackpacks_gold_upgarde",<improvedbackpacks:upgrade:3>,
	[[null,<betterwithmods:material:6>,null],
	 [<immersiveengineering:metal:40>,<improvedbackpacks:upgrade:2>,<immersiveengineering:metal:40>],
	 [null,<immersiveengineering:metal:40>,null]]);
recipes.remove(<improvedbackpacks:upgrade:4>);
recipes.addShaped("improvedbackpacks_diamound_upgarde",<improvedbackpacks:upgrade:4>,
	[[null,<betterwithmods:material:6>,null],
	 [<primal:diamond_plate>,<improvedbackpacks:upgrade:3>,<primal:diamond_plate>],
	 [null,<primal:diamond_plate>,null]]);
