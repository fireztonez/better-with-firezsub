import crafttweaker.item.IItemStack;

var furnaceOutputRemove = [
  <industrialforegoing:plastic>,
  <minecraft:iron_ingot>,
  <minecraft:gold_ingot>,
  <astralsorcery:itemcraftingcomponent:1>,
  <immersiveengineering:metal>,
  <immersiveengineering:metal:1>,
  <immersiveengineering:metal:2>,
  <immersiveengineering:metal:3>,
  <immersiveengineering:metal:4>,
  <immersiveengineering:metal:6>,
  <immersiveengineering:metal:7>,
  <immersiveengineering:metal:8>,
  <immersiveengineering:metal:5>,
  <primal:crude_iron_ingot>,
  <primal:wootz_ingot>,
  <primal:tamahagane_ingot>,
  <primal:vanadium_ingot>,
  <primal:steel_ingot>,
  <primal:copper_ingot>,
  <primal:tin_ingot>,
  <primal:bronze_ingot>,
  <primal:brass_ingot>,
  <primal:silver_ingot>,
  <primal:lead_ingot>,
  <primal:platinum_ingot>,
  <primal:zinc_ingot>,
  <tconstruct:ingots:2>,
  <abyssalcraft:cpearl>,
  <mysticalagriculture:crafting:38>,
  <geolosys:ingot:6>,
  <rockhounding_chemistry:metal_items:2>,
  <rockhounding_chemistry:metal_items:3>,
  <rockhounding_chemistry:metal_items:5>,
  <rockhounding_chemistry:metal_items:6>,
  <rockhounding_chemistry:metal_items:9>,
  <rockhounding_chemistry:metal_items:10>,
  <rockhounding_chemistry:metal_items:13>,
  <mysticalagriculture:crafting:38>
] as IItemStack[];

for item in furnaceOutputRemove {
  furnace.remove(item);
}
