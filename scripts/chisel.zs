import mods.chisel.Carving;

Carving.addGroup("common_stone");
Carving.addVariation("common_stone", <primal:common_stone>);
Carving.addVariation("common_stone", <primal:common_stone:1>);
Carving.addVariation("common_stone", <primal:common_stone:2>);
Carving.addVariation("common_stone", <primal:common_stone:3>);
Carving.addVariation("common_stone", <primal:common_stone:4>);
Carving.addVariation("common_stone", <primal:common_stone:5>);
Carving.addVariation("common_stone", <primal:common_stone:6>);
Carving.addVariation("common_stone", <primal:common_stone:7>);

Carving.addGroup("sarsen_stone");
Carving.addVariation("sarsen_stone", <primal:sarsen_stone>);
Carving.addVariation("sarsen_stone", <primal:sarsen_stone:1>);
Carving.addVariation("sarsen_stone", <primal:sarsen_stone:2>);
Carving.addVariation("sarsen_stone", <primal:sarsen_stone:3>);
Carving.addVariation("sarsen_stone", <primal:sarsen_stone:4>);
Carving.addVariation("sarsen_stone", <primal:sarsen_stone:5>);
Carving.addVariation("sarsen_stone", <primal:sarsen_stone:6>);
Carving.addVariation("sarsen_stone", <primal:sarsen_stone:7>);

Carving.addGroup("blue_stone");
Carving.addVariation("blue_stone", <primal:blue_stone>);
Carving.addVariation("blue_stone", <primal:blue_stone:1>);
Carving.addVariation("blue_stone", <primal:blue_stone:2>);
Carving.addVariation("blue_stone", <primal:blue_stone:3>);
Carving.addVariation("blue_stone", <primal:blue_stone:4>);
Carving.addVariation("blue_stone", <primal:blue_stone:5>);
Carving.addVariation("blue_stone", <primal:blue_stone:6>);
Carving.addVariation("blue_stone", <primal:blue_stone:7>);

Carving.addGroup("ortho_stone");
Carving.addVariation("ortho_stone", <primal:ortho_stone>);
Carving.addVariation("ortho_stone", <primal:ortho_stone:1>);
Carving.addVariation("ortho_stone", <primal:ortho_stone:2>);
Carving.addVariation("ortho_stone", <primal:ortho_stone:3>);
Carving.addVariation("ortho_stone", <primal:ortho_stone:4>);
Carving.addVariation("ortho_stone", <primal:ortho_stone:5>);
Carving.addVariation("ortho_stone", <primal:ortho_stone:6>);
Carving.addVariation("ortho_stone", <primal:ortho_stone:7>);

Carving.addGroup("schist_green_stone");
Carving.addVariation("schist_green_stone", <primal:schist_green_stone>);
Carving.addVariation("schist_green_stone", <primal:schist_green_stone:1>);
Carving.addVariation("schist_green_stone", <primal:schist_green_stone:2>);
Carving.addVariation("schist_green_stone", <primal:schist_green_stone:3>);
Carving.addVariation("schist_green_stone", <primal:schist_green_stone:4>);
Carving.addVariation("schist_green_stone", <primal:schist_green_stone:5>);
Carving.addVariation("schist_green_stone", <primal:schist_green_stone:6>);
Carving.addVariation("schist_green_stone", <primal:schist_green_stone:7>);

Carving.addGroup("schist_blue_stone");
Carving.addVariation("schist_blue_stone", <primal:schist_blue_stone>);
Carving.addVariation("schist_blue_stone", <primal:schist_blue_stone:1>);
Carving.addVariation("schist_blue_stone", <primal:schist_blue_stone:2>);
Carving.addVariation("schist_blue_stone", <primal:schist_blue_stone:3>);
Carving.addVariation("schist_blue_stone", <primal:schist_blue_stone:4>);
Carving.addVariation("schist_blue_stone", <primal:schist_blue_stone:5>);
Carving.addVariation("schist_blue_stone", <primal:schist_blue_stone:6>);
Carving.addVariation("schist_blue_stone", <primal:schist_blue_stone:7>);

Carving.addGroup("scoria_stone");
Carving.addVariation("scoria_stone", <primal:scoria_stone>);
Carving.addVariation("scoria_stone", <primal:scoria_stone:1>);
Carving.addVariation("scoria_stone", <primal:scoria_stone:2>);
Carving.addVariation("scoria_stone", <primal:scoria_stone:3>);
Carving.addVariation("scoria_stone", <primal:scoria_stone:4>);
Carving.addVariation("scoria_stone", <primal:scoria_stone:5>);
Carving.addVariation("scoria_stone", <primal:scoria_stone:6>);
Carving.addVariation("scoria_stone", <primal:scoria_stone:7>);

Carving.addGroup("porphyry_stone");
Carving.addVariation("porphyry_stone", <primal:porphyry_stone>);
Carving.addVariation("porphyry_stone", <primal:porphyry_stone:1>);
Carving.addVariation("porphyry_stone", <primal:porphyry_stone:2>);
Carving.addVariation("porphyry_stone", <primal:porphyry_stone:3>);
Carving.addVariation("porphyry_stone", <primal:porphyry_stone:4>);
Carving.addVariation("porphyry_stone", <primal:porphyry_stone:5>);
Carving.addVariation("porphyry_stone", <primal:porphyry_stone:6>);
Carving.addVariation("porphyry_stone", <primal:porphyry_stone:7>);

Carving.addGroup("purpurite_stone");
Carving.addVariation("purpurite_stone", <primal:purpurite_stone>);
Carving.addVariation("purpurite_stone", <primal:purpurite_stone:1>);
Carving.addVariation("purpurite_stone", <primal:purpurite_stone:2>);
Carving.addVariation("purpurite_stone", <primal:purpurite_stone:3>);
Carving.addVariation("purpurite_stone", <primal:purpurite_stone:4>);
Carving.addVariation("purpurite_stone", <primal:purpurite_stone:5>);
Carving.addVariation("purpurite_stone", <primal:purpurite_stone:6>);
Carving.addVariation("purpurite_stone", <primal:purpurite_stone:7>);

Carving.addGroup("ferro_stone");
Carving.addVariation("ferro_stone", <primal:ferro_stone>);
Carving.addVariation("ferro_stone", <primal:ferro_stone:1>);
Carving.addVariation("ferro_stone", <primal:ferro_stone:2>);
Carving.addVariation("ferro_stone", <primal:ferro_stone:3>);
Carving.addVariation("ferro_stone", <primal:ferro_stone:4>);
Carving.addVariation("ferro_stone", <primal:ferro_stone:5>);
Carving.addVariation("ferro_stone", <primal:ferro_stone:6>);
Carving.addVariation("ferro_stone", <primal:ferro_stone:7>);

Carving.addGroup("carbonate_stone");
Carving.addVariation("carbonate_stone", <primal:carbonate_stone>);
Carving.addVariation("carbonate_stone", <primal:carbonate_stone:1>);
Carving.addVariation("carbonate_stone", <primal:carbonate_stone:2>);
Carving.addVariation("carbonate_stone", <primal:carbonate_stone:3>);
Carving.addVariation("carbonate_stone", <primal:carbonate_stone:4>);
Carving.addVariation("carbonate_stone", <primal:carbonate_stone:5>);
Carving.addVariation("carbonate_stone", <primal:carbonate_stone:6>);
Carving.addVariation("carbonate_stone", <primal:carbonate_stone:7>);

Carving.addGroup("nether_stone");
Carving.addVariation("nether_stone", <primal:nether_stone>);
Carving.addVariation("nether_stone", <primal:nether_stone:1>);
Carving.addVariation("nether_stone", <primal:nether_stone:2>);
Carving.addVariation("nether_stone", <primal:nether_stone:3>);
Carving.addVariation("nether_stone", <primal:nether_stone:4>);
Carving.addVariation("nether_stone", <primal:nether_stone:5>);
Carving.addVariation("nether_stone", <primal:nether_stone:6>);
Carving.addVariation("nether_stone", <primal:nether_stone:7>);

Carving.addGroup("eroded_end_stone");
Carving.addVariation("eroded_end_stone", <primal:eroded_end_stone>);
Carving.addVariation("eroded_end_stone", <primal:eroded_end_stone:1>);
Carving.addVariation("eroded_end_stone", <primal:eroded_end_stone:2>);
Carving.addVariation("eroded_end_stone", <primal:eroded_end_stone:3>);
Carving.addVariation("eroded_end_stone", <primal:eroded_end_stone:4>);
Carving.addVariation("eroded_end_stone", <primal:eroded_end_stone:5>);
Carving.addVariation("eroded_end_stone", <primal:eroded_end_stone:6>);
Carving.addVariation("eroded_end_stone", <primal:eroded_end_stone:7>);

Carving.addGroup("desiccated_stone");
Carving.addVariation("desiccated_stone", <primal:desiccated_stone>);
Carving.addVariation("desiccated_stone", <primal:desiccated_stone:1>);
Carving.addVariation("desiccated_stone", <primal:desiccated_stone:2>);
Carving.addVariation("desiccated_stone", <primal:desiccated_stone:3>);
Carving.addVariation("desiccated_stone", <primal:desiccated_stone:4>);
Carving.addVariation("desiccated_stone", <primal:desiccated_stone:5>);
Carving.addVariation("desiccated_stone", <primal:desiccated_stone:6>);
Carving.addVariation("desiccated_stone", <primal:desiccated_stone:7>);

Carving.addGroup("soul_stone");
Carving.addVariation("soul_stone", <primal:soul_stone>);
Carving.addVariation("soul_stone", <primal:soul_stone:1>);
Carving.addVariation("soul_stone", <primal:soul_stone:2>);
Carving.addVariation("soul_stone", <primal:soul_stone:3>);
Carving.addVariation("soul_stone", <primal:soul_stone:4>);
Carving.addVariation("soul_stone", <primal:soul_stone:5>);
Carving.addVariation("soul_stone", <primal:soul_stone:6>);
Carving.addVariation("soul_stone", <primal:soul_stone:7>);

Carving.addGroup("night_stone");
Carving.addVariation("night_stone", <primal:night_stone>);
Carving.addVariation("night_stone", <primal:night_stone:1>);
Carving.addVariation("night_stone", <primal:night_stone:2>);
Carving.addVariation("night_stone", <primal:night_stone:3>);
Carving.addVariation("night_stone", <primal:night_stone:4>);
Carving.addVariation("night_stone", <primal:night_stone:5>);
Carving.addVariation("night_stone", <primal:night_stone:6>);
Carving.addVariation("night_stone", <primal:night_stone:7>);

Carving.addVariation("basalt", <quark:basalt>);
Carving.addVariation("basalt", <quark:basalt:1>);
Carving.addVariation("basalt", <quark:world_stone_pavement:3>);

//Add All TCon Glasses in Chisel...
Carving.addVariation("glass", <tconstruct:clear_glass>);
Carving.addVariation("glassdyedwhite", <tconstruct:clear_stained_glass>);
Carving.addVariation("glassdyedorange", <tconstruct:clear_stained_glass:1>);
Carving.addVariation("glassdyedmagenta", <tconstruct:clear_stained_glass:2>);
Carving.addVariation("glassdyedlightblue", <tconstruct:clear_stained_glass:3>);
Carving.addVariation("glassdyedyellow", <tconstruct:clear_stained_glass:4>);
Carving.addVariation("glassdyedlime", <tconstruct:clear_stained_glass:5>);
Carving.addVariation("glassdyedpink", <tconstruct:clear_stained_glass:6>);
Carving.addVariation("glassdyedgray", <tconstruct:clear_stained_glass:7>);
Carving.addVariation("glassdyedlightgray", <tconstruct:clear_stained_glass:8>);
Carving.addVariation("glassdyedcyan", <tconstruct:clear_stained_glass:9>);
Carving.addVariation("glassdyedpurple", <tconstruct:clear_stained_glass:10>);
Carving.addVariation("glassdyedblue", <tconstruct:clear_stained_glass:11>);
Carving.addVariation("glassdyedbrown", <tconstruct:clear_stained_glass:12>);
Carving.addVariation("glassdyedgreen", <tconstruct:clear_stained_glass:13>);
Carving.addVariation("glassdyedred", <tconstruct:clear_stained_glass:14>);
Carving.addVariation("glassdyedblack", <tconstruct:clear_stained_glass:15>);


//Fix Chisel Bug with Gold Blocks
Carving.addVariation("blockGold", <minecraft:gold_block>);
Carving.addVariation("blockGold", <chisel:blockgold>);
Carving.addVariation("blockGold", <chisel:blockgold:1>);
Carving.addVariation("blockGold", <chisel:blockgold:2>);
Carving.addVariation("blockGold", <chisel:blockgold:3>);
Carving.addVariation("blockGold", <chisel:blockgold:4>);
Carving.addVariation("blockGold", <chisel:blockgold:5>);
Carving.addVariation("blockGold", <chisel:blockgold:6>);

Carving.addGroup("slate");
Carving.addVariation("slate", <rustic:slate_pillar>);
Carving.addVariation("slate", <rustic:slate>);
Carving.addVariation("slate", <rustic:slate_roof>);
Carving.addVariation("slate", <rustic:slate_brick>);
Carving.addVariation("slate", <rustic:slate_chiseled>);
