import crafttweaker.item.IIngredient;
import crafttweaker.item.IItemStack;

var STAGE = STAGES.starting;


var replacementItemsForStage as IIngredient[][][string] = {
  STAGES.advanced : [
    [samplePlatinum, sampleDust],
    [sampleAutunite, sampleDust],
    [sampleKimberlite, sampleDust],
    [sampleBeryl, sampleDust],
    [oreBlockPlatinum, stone],
    [oreBlockAutunite, stone],
    [oreBlockKimberlite, stone],
    [oreBlockBeryl, stone],
    [oreDiamond, stone],
    [oreEmerald, stone]
  ],
  STAGES.nuclear : [
    [sampleFluorite, sampleDust],
    [sampleZircon, sampleDust],
    [sampleBertrandite, sampleDust],
    [sampleVilliaumite, sampleDust],
    [sampleCarobbiite, sampleDust],
    [sampleRhodochrosite, sampleDust],
    [sampleThorium, sampleDust],
    [sampleMagnesium, sampleDust],
    [sampleLithium, sampleDust],
    [sampleBoron, sampleDust],
    [sampleAutunite, sampleDust],

    [oreFluorite, stone],
    [oreZircon, stone],
    [oreBertrandite, stone],
    [oreVilliaumite, stone],
    [oreCarobbiite, stone],
    [oreRhodochrosite, stone],
    [oreThorium, stone],
    [oreMagnesium, stone],
    [oreLithium, stone],
    [oreBoron, stone],
    [oreBlockAutunite, stone]

  ]
};

var mobsForStage as string[][string] = {
  STAGES.advanced : [
    "minecraft:creeper"
  ],
  STAGES.nuclear : [
    "minecraft:ghast"
  ]
};

for stage, itemReplacementPairs in replacementItemsForStage {
	for itemReplacementPair in itemReplacementPairs {
		var length = itemReplacementPair.length;

		if (length == 1) {
			mods.orestages.OreStages.addReplacement(stage, itemReplacementPair[0]);
		} else if (length == 2) {
			mods.orestages.OreStages.addReplacement(stage, itemReplacementPair[0], itemReplacementPair[1].items[0]);
		}
	}
}
