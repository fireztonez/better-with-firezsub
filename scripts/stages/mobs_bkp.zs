import mods.MobStages;

/*
	Mob Stages

	http://crafttweaker.readthedocs.io/en/latest/#Mods/GameStages/MobStages/MobStages/#global-options
*/
	//Stage one

//mods.MobStages.addStage("one", "minecraft:skeleton");
//mods.MobStages.addStage("one", "minecraft:skeleton_horse");
//
//	//Stage two
//mods.MobStages.addStage("two", "minecraft:evocation_illager");
//mods.MobStages.addStage("two", "minecraft:silverfish");
//mods.MobStages.addStage("two", "minecraft:vindication_illager");
//mods.MobStages.addStage("two", "minecraft:vex");
//mods.MobStages.addStage("two", "abyssalcraft:shadowmonster");
//mods.MobStages.addStage("two", "abyssalcraft:shadowbeast");
//mods.MobStages.addStage("two", "abyssalcraft:abyssalzombie");
//mods.MobStages.addStage("two", "abyssalcraft:lessershoggoth");
//mods.MobStages.addStage("two", "abyssalcraft:depthsghoul");
//mods.MobStages.addStage("two", "enderutilities:endermanfighter");
//mods.MobStages.addStage("two", "minecraft:wither_skeleton");
//mods.MobStages.addStage("two", "thermalfoundation:blitz");
//mods.MobStages.addStage("two", "thermalfoundation:basalz");
//mods.MobStages.addStage("two", "thermalfoundation:blizz");
//
//	//Stage three
//mods.MobStages.addStage("three", "cyberware:cyberzombie");

	//Stage four
//mods.MobStages.addStage("two", "matteroverdrive:rogue_android");
//mods.MobStages.addStage("two", "matteroverdrive:ranged_rogue_android");
//mods.MobStages.addStage("two", "matteroverdrive:drone");
