import mods.MobStages;

import mods.zenstages.Stage;
import mods.zenstages.ZenStager;

import scripts.stages._stages.advancedStage;
import scripts.stages._stages.roboticStage;
import scripts.stages._stages.nuclearStage;
import scripts.stages._stages.matterStage;

// ==================================
// Mobs Staging
//static mobsMap as string[][string] = {
//	advancedStage.stage: [
//        "minecraft:evocation_illager",
//        "minecraft:shulker",
//        "minecraft:silverfish",
//        "minecraft:vindication_illager",
//        "minecraft:vex",
//        "minecraft:zombie",
//        "abyssalcraft:shadowmonster",
//        "abyssalcraft:shadowbeast",
//        "abyssalcraft:abyssalzombie",
//        "abyssalcraft:lessershoggoth",
//        "abyssalcraft:depthsghoul",
//        "enderutilities:endermanfighter",
//        "minecraft:wither_skeleton",
//        "thermalfoundation:blitz",
//        "thermalfoundation:basalz",
//        "thermalfoundation:blizz"
//	],
//	roboticStage.stage: [
//		"cyberware:cyberzombie"
//	],
//
//	nuclearStage.stage: [
//	]
//	matterStage.stage: [
//        "matteroverdrive:failed_pig",
//        "matteroverdrive:failed_cow",
//        "matteroverdrive:failed_chicken",
//        "matteroverdrive:failed_sheep",
//        "matteroverdrive:mutant_scientist",
//        "matteroverdrive:rogue_android",
//        "matteroverdrive:ranged_rogue_android",
//        "matteroverdrive:drone"
//	]
//};
//
///*
//	Init method to perform the logic for the script.
//*/
//function init() {
//	for _stage, mobs in mobsMap {
//		ZenStager.getStage(_stage).addMobs(mobs);
//	}
//}
//mods.MobStages.addStage("one", "minecraft:creeper");
//
//
//static mobsForStages as string[][string] = {
//	STAGES.advanced: [
mods.MobStages.addStage("advanced", "minecraft:evocation_illager");
mods.MobStages.addStage("advanced", "minecraft:shulker");
mods.MobStages.addStage("advanced", "minecraft:silverfish");
mods.MobStages.addStage("advanced", "minecraft:vindication_illager");
mods.MobStages.addStage("advanced", "minecraft:vex");
mods.MobStages.addStage("advanced", "abyssalcraft:shadowmonster");
mods.MobStages.addStage("advanced", "abyssalcraft:shadowbeast");
mods.MobStages.addStage("advanced", "abyssalcraft:abyssalzombie");
mods.MobStages.addStage("advanced", "abyssalcraft:lessershoggoth");
mods.MobStages.addStage("advanced", "abyssalcraft:depthsghoul");
mods.MobStages.addStage("advanced", "enderutilities:endermanfighter");
mods.MobStages.addStage("advanced", "minecraft:wither_skeleton");
mods.MobStages.addStage("advanced", "thermalfoundation:blitz");
mods.MobStages.addStage("advanced", "thermalfoundation:basalz");
mods.MobStages.addStage("advanced", "thermalfoundation:blizz");


mods.MobStages.addStage("matter", "matteroverdrive:rogue_android");
mods.MobStages.addStage("matter", "matteroverdrive:ranged_rogue_android");
//	],
//	STAGES.robotic: [
//		"cyberware:cyberzombie"
//	],
//};

//	STAGES.matter: [
//        "matteroverdrive:failed_pig",
//        "matteroverdrive:failed_cow",
//        "matteroverdrive:failed_chicken",
//        "matteroverdrive:failed_sheep",
//        "matteroverdrive:mutant_scientist",
//        "matteroverdrive:rogue_android",
//        "matteroverdrive:ranged_rogue_android",
//        "matteroverdrive:drone"
//	]
//};

//for stage, mobs in mobsForStages {
//	for mob in mobs {
//		MobStages.addStage(stage, mob);
//	}
//}
