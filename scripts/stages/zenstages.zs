import mods.zenstages.ZenStager;
import mods.zenstages.Stage;

var startingStage = ZenStager.initStage("starting");
var advancedStage = ZenStager.initStage("advanced");
var nuclearStage = ZenStager.initStage("nuclear");
var matterStage = ZenStager.initStage("matter");

ZenStager.isStaged("mob", "minecraft:skeleton");
