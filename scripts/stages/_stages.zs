#priority 3999

import mods.zenstages.ZenStager;
import mods.zenstages.Stage;

import mods.MobStages;

// Core Stages
static startingStage as Stage = ZenStager.initStage("starting");
static advancedStage as Stage = ZenStager.initStage("advanced");
static roboticStage as Stage = ZenStager.initStage("robotic");
static nuclearStage as Stage = ZenStager.initStage("nuclear");
static matterStage as Stage = ZenStager.initStage("matter");

// Unique stage intended to disable existing items/etc
static stageDisabled as Stage = ZenStager.initStage("disabled");

static stageNoMatter as Stage = ZenStager.initStage("no-matter");
