import crafttweaker.oredict.IOreDictEntry;
#priority 990


<ore:clumpFuel>.add(<betterwithmods:material:1>);

<ore:clumpCharcoal>.add(<primal:charcoal_fair>,<primal:charcoal_good>,<primal:charcoal_high>,<minecraft:coal:1>);
<ore:clumpCoal>.add(<minecraft:coal>,<betterwithmods:material:1>);
<ore:clumpCoal>.add(<minecraft:coal>,<betterwithmods:material:1>);

<ore:tallow>.remove(<rustic:tallow>,<rustic:beeswax>);

<ore:leatherTanned>.add(<betterwithmods:material:6>,<betterwithmods:material:32>);
<ore:hideTanned>.remove(<betterwithmods:material:6>,<betterwithmods:material:32>);

<ore:castGems>.add(<minecraft:diamond>,<minecraft:quartz>,<appliedenergistics2:material>,<appliedenergistics2:material:1>,<appliedenergistics2:material:11>,<appliedenergistics2:material:10>);

<ore:logOak>.add(<twilightforest:twilight_log>);
<ore:logSpruce>.add(<twilightforest:twilight_log:1>,<twilightforest:twilight_log:3>);
<ore:logBirch>.add(<twilightforest:twilight_log:2>);

<ore:ingotSteel>.remove(<primal:wootz_ingot>,<primal:tamahagane_ingot>);
<ore:plateSteel>.remove(<primal:wootz_plate>);
<ore:plateSteel>.remove(<primal:wootz_plate>);

<ore:oreThorium>.add(<contenttweaker:material_part:52>);
<ore:oreBoron>.add(<contenttweaker:material_part:54>);
<ore:oreLithium>.add(<contenttweaker:material_part:56>);
<ore:oreMagnesium>.add(<contenttweaker:material_part:58>);

<ore:plateSteel>.remove(<primal:tamahagane_plate>);

<ore:clusterBe>.remove(clusterBeryllium);
<ore:clusterBeryllium>.add(clusterBeryllium);
<ore:oreBeryllium>.add(clusterBeryllium);

<ore:oreZinc>.add(<geolosys:ore:10>);

<ore:plateBlackIron>.add(<extendedcrafting:material:2>);

<ore:stoneBasalt>.add(<quark:world_stone_bricks:3>, <quark:basalt:1>, <quark:world_stone_pavement:3>);

<ore:blockGlassColorless>.add(<tconstruct:clear_glass>);
//<ore:dustQuartz>.add(<nuclearcraft:gem_dust:2>);
//<ore:dustNetherQuartz>.add(<nuclearcraft:gem_dust:2>);
