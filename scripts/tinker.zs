recipes.remove(<tconstruct:slime_boots:*>);
recipes.remove(<tconstruct:toolforge>);

mods.jei.JEI.addDescription(<tconstruct:toolforge>.withTag({textureBlock: {id: "twilightforest:block_storage", Count: 1, Damage: 2}}), "To get Tool Forge, you need to infuse a Tool Station with the Startlight Infuser from Astral Sorcery.", "", "The recipe will only show in JEI when you look the Usage of Tool Station.");
