import crafttweaker.item.IItemStack;

var disabled = [
    <minecraft:shield>,
    <extrarails:teleporting_rail>,
    <storagedrawers:controllerslave>,
    <ironjetpacks:wood_jetpack>,
    <ironjetpacks:wood_thruster>,
    <improvedbackpacks:bound_leather>,
    <industrialforegoing:latex_processing_unit>,
    <modularmachinery:blockoutputbus>,
    <modularmachinery:blockinputbus>,
    <minecraft:potion>.withTag({Potion: "minecraft:luck"}),
    <microblockcbe:stone_rod>,
    <pneumaticcraft:spawner_agitator>
    //<primal:flint_hoe>,
    //<minecraft:stone_hoe>,
    //<minecraft:wooden_hoe>,
    //<cyclicmagic:netherbrick_hoe>,
    //<appliedenergistics2:nether_quartz_hoe>,
    //<appliedenergistics2:certus_quartz_hoe>,
    //<primal:bone_hoe>,
    //<primal:bone_hatchet>,
    //<actuallyadditions:block_misc:5>
] as IItemStack[];

for i, item in disabled {
    mods.jei.JEI.removeAndHide(item);
}

var hide = [
    <nuclearcraft:ore>,
    <nuclearcraft:ore:2>,
    <nuclearcraft:ore:4>
] as IItemStack[];

for i, item in hide {
    mods.jei.JEI.hide(item);
}


mods.jei.JEI.hide(<betterwithmods:siding_wood>.withTag({texture: {Properties: {type: "ironwood"}, Name: "primal:planks"}}));
mods.jei.JEI.hide(<betterwithmods:moulding_wood>.withTag({texture: {Properties: {type: "ironwood"}, Name: "primal:planks"}}));
mods.jei.JEI.hide(<betterwithmods:corner_wood>.withTag({texture: {Properties: {type: "ironwood"}, Name: "primal:planks"}}));
