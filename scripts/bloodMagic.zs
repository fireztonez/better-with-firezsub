
recipes.remove(<bloodmagic:soul_forge>);
recipes.addShaped("bloodmagic_soulfourge_gated",<bloodmagic:soul_forge>,
    [[<astralsorcery:itemrockcrystalsimple>, null, <astralsorcery:itemrockcrystalsimple>],
     [<ore:stoneBasalt>, metals.bronze.ingot, <ore:stoneBasalt>],
     [<ore:stoneBasalt>, metals.steel.block, <ore:stoneBasalt>]]);
