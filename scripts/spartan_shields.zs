import crafttweaker.item.IItemStack;
import crafttweaker.item.IItemDefinition;
import crafttweaker.item.IIngredient;
import mods.betterwithmods.MiniBlocks;
import crafttweaker.oredict.IOreDictEntry;
import crafttweaker.liquid.ILiquidStack;
import mods.tconstruct.Casting;

recipes.remove(<spartanshields:shield_basic_wood>);
recipes.remove(<spartanshields:shield_basic_stone>);
recipes.remove(<spartanshields:shield_basic_iron>);
recipes.remove(<spartanshields:shield_basic_gold>);
recipes.remove(<spartanshields:shield_basic_diamond>);
recipes.remove(<spartanshields:shield_basic_obsidian>);
recipes.remove(<spartanshields:shield_basic_soulforged_steel>);

recipes.addShapedMirrored("spartanshields_wood",<spartanshields:shield_basic_wood>,
    [[null, sidingPlankWood, sidingPlankWood],
     [sidingPlankWood, <ore:thinWood>, sidingPlankWood],
     [sidingPlankWood, sidingPlankWood, null]]);

recipes.addShapedMirrored("spartanshields_stone",<spartanshields:shield_basic_stone>,
    [[null, <ore:stone>, null],
     [<ore:stone>, <spartanshields:shield_basic_wood>, <ore:stone>],
     [null, <ore:stone>, null]]);

Casting.addTableRecipe(<spartanshields:shield_basic_gold>, <spartanshields:shield_basic_stone>, <liquid:gold>, 576, true);
Casting.addTableRecipe(<spartanshields:shield_basic_iron>, <spartanshields:shield_basic_gold>, <liquid:iron>, 576, true);
Casting.addTableRecipe(<spartanshields:shield_basic_obsidian>, <spartanshields:shield_basic_iron>, <liquid:obsidian>, 576, true);

recipes.addShapedMirrored("spartanshields_stone",<spartanshields:shield_basic_diamond>,
    [[null, diamondIngot, null],
    [diamondIngot, <spartanshields:shield_basic_iron>, diamondIngot],
    [null, diamondIngot, null]]);

    Casting.addTableRecipe(<spartanshields:shield_basic_soulforged_steel>, <spartanshields:shield_basic_diamond>, <liquid:soulforged_steel>, 576, true);
