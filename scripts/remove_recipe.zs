import crafttweaker.item.IItemStack;

var recipesOutputRemove = [
  <enderutilities:enderpart>,
  <enderutilities:enderpart:1>,
  <enderutilities:enderpart:2>,
  <cyclicmagic:netherbrick_pickaxe>,
  <cyclicmagic:sandstone_pickaxe>,
  <cyclicmagic:sandstone_axe>,
  <cyclicmagic:sandstone_spade>,
  <cyclicmagic:sandstone_hoe>,
  <ceramics:unfired_clay>,
  <minecraft:bucket>,
  <chisel:block_charcoal2:1>,
  <quark:charcoal_block>,
  <primal:brick_mold:*>,
  <immersiveengineering:tool>,
  <actuallyadditions:item_food:9>,
  <minecraft:sugar>,
  <extendedcrafting:material:14>,
  <extendedcrafting:material:15>,
  <extendedcrafting:material:16>,
  <extendedcrafting:material:17>,
  <extendedcrafting:material:7>,
  <minecraft:glass>,
  <minecraft:stained_glass>,
  <minecraft:stained_glass:1>,
  <minecraft:stained_glass:2>,
  <minecraft:stained_glass:3>,
  <minecraft:stained_glass:4>,
  <minecraft:stained_glass:5>,
  <minecraft:stained_glass:6>,
  <minecraft:stained_glass:7>,
  <minecraft:stained_glass:8>,
  <minecraft:stained_glass:9>,
  <minecraft:stained_glass:10>,
  <minecraft:stained_glass:11>,
  <minecraft:stained_glass:12>,
  <minecraft:stained_glass:13>,
  <minecraft:stained_glass:14>,
  <minecraft:stained_glass:15>,
  <thermalfoundation:material:97>,
  <thermalfoundation:material:98>,
  <thermalfoundation:material:100>,
  <thermalfoundation:material:101>,
  <thermalfoundation:material:102>,
  <thermalfoundation:material:103>,
  <quark:world_stone_pavement:3>,
  <quark:world_stone_bricks:3>,
  <quark:basalt:1>,
  <oeintegration:excavatemodifier>,
  <primal:slat_iron>,
  <nuclearcraft:part>,
  <nuclearcraft:part:1>,
  <nuclearcraft:part:2>,
  <nuclearcraft:part:3>
] as IItemStack[];

var recipesNameRemove = [
  "ceramics:decoration/unfired_porcelain_quartz",
  "ceramics:decoration/unfired_porcelain_bone_meal",
  "ceramics:decoration/porcelain_barrel",
  "thermalfoundation:gunpowder",
  "thermalfoundation:gunpowder_1",
  "immersiveengineering:material/gunpowder0",
  "immersiveengineering:material/gunpowder1"
] as string[];

for item in recipesOutputRemove {
  recipes.remove(item);
}

for name in recipesNameRemove {
  recipes.removeByRegex(name);
}
