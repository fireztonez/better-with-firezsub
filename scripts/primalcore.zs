import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import crafttweaker.oredict.IOreDictEntry;

var brickMoldIngredientArray = [<primal:logs_split_oak>,<primal:logs_split_spruce>,<primal:logs_split_birch>,<primal:logs_split_jungle>,<primal:logs_split_acacia>,<primal:logs_split_bigoak>,<primal:logs_split_ironwood>,<primal:logs_split_yew>,<primal:planks:2>,<primal:planks:3>] as IItemStack[];
var brickMoldDef = <primal:brick_mold>.definition;
for i, item in brickMoldIngredientArray {
	 recipes.addShaped("primal_bricksMold_" + i, brickMoldDef.makeStack(i),
    [[item,null,item],
     [item,item,item],
     [item,item,item]]);
}

recipes.addShaped("primalcore_slat_iron", <primal:slat_iron>, [[<ore:toolMallet>], [<minecraft:iron_bars>]]);
