#priority 900
import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import crafttweaker.oredict.IOreDictEntry;
import mods.betterwithmods.MiniBlocks;

recipes.remove(<storagedrawers:customdrawers:*>);

#Storage Drawers
var basicDrawerRecipes = recipes.getRecipesFor(<storagedrawers:basicdrawers:0>);
var oneByTwoDrawerRecipes = recipes.getRecipesFor(<storagedrawers:basicdrawers:1>);
var twoByTwoDrawerRecipes = recipes.getRecipesFor(<storagedrawers:basicdrawers:2>);
var oneByTwoHalfDrawerRecipes = recipes.getRecipesFor(<storagedrawers:basicdrawers:3>);
var twoByTwoHalfDrawerRecipes = recipes.getRecipesFor(<storagedrawers:basicdrawers:4>);

#This is the easiest way to do this even if it is ugly
for iter, drawerRecipe in basicDrawerRecipes {
    recipes.remove(drawerRecipe.output, true);
}

for iter, drawerRecipe in oneByTwoDrawerRecipes {
    recipes.remove(drawerRecipe.output, true);
}

for iter, drawerRecipe in twoByTwoDrawerRecipes {
    recipes.remove(drawerRecipe.output, true);
}

for iter, drawerRecipe in oneByTwoHalfDrawerRecipes {
    recipes.remove(drawerRecipe.output, true);
}

for iter, drawerRecipe in twoByTwoHalfDrawerRecipes {
    recipes.remove(drawerRecipe.output, true);
}

recipes.remove(<storagedrawers:upgrade_template>);
recipes.addShaped("drawers_upgrade_template",<storagedrawers:upgrade_template>,
	[[<ore:stickWood>,<ore:stickWood>,<ore:stickWood>],
	 [<betterwithmods:material>,<storagedrawers:customdrawers:*>,<betterwithmods:material>],
	 [<ore:stickWood>,<ore:stickWood>,<ore:stickWood>]]);

recipes.remove(<storagedrawers:upgrade_storage>);
recipes.addShaped("drawers_upgrade_storage_1",<storagedrawers:upgrade_storage>,
	[[<ore:stickWood>,<ore:stickWood>,<ore:stickWood>],
	 [<ore:obsidian>,<storagedrawers:upgrade_template>,<ore:obsidian>],
	 [<ore:stickWood>,<ore:stickWood>,<ore:stickWood>]]);

recipes.remove(<storagedrawers:upgrade_storage:1>);
recipes.addShaped("drawers_upgrade_storage_2",<storagedrawers:upgrade_storage:1>,
	[[<ore:stickWood>,<ore:stickWood>,<ore:stickWood>],
	 [<ore:plateIron>,<storagedrawers:upgrade_storage>,<ore:plateIron>],
	 [<ore:stickWood>,<ore:stickWood>,<ore:stickWood>]]);

recipes.remove(<storagedrawers:upgrade_storage:2>);
recipes.addShaped("drawers_upgrade_storage_3",<storagedrawers:upgrade_storage:2>,
	[[<ore:stickWood>,<ore:stickWood>,<ore:stickWood>],
	 [<ore:plateGold>,<storagedrawers:upgrade_storage:1>,<ore:plateGold>],
	 [<ore:stickWood>,<ore:stickWood>,<ore:stickWood>]]);

recipes.remove(<storagedrawers:upgrade_storage:3>);
recipes.addShaped("drawers_upgrade_storage_4",<storagedrawers:upgrade_storage:3>,
	[[<ore:stickWood>,<ore:stickWood>,<ore:stickWood>],
	 [<ore:plateDiamond>,<storagedrawers:upgrade_storage:2>,<ore:plateDiamond>],
	 [<ore:stickWood>,<ore:stickWood>,<ore:stickWood>]]);

recipes.remove(<storagedrawers:upgrade_storage:4>);
recipes.addShaped("drawers_upgrade_storage_5",<storagedrawers:upgrade_storage:4>,
	[[<ore:stickWood>,<ore:stickWood>,<ore:stickWood>],
	 [<minecraft:emerald>,<storagedrawers:upgrade_storage:3>,<minecraft:emerald>],
	 [<ore:stickWood>,<ore:stickWood>,<ore:stickWood>]]);

recipes.remove(<storagedrawers:upgrade_one_stack>);
recipes.addShaped("drawers_upgrade_onestack",<storagedrawers:upgrade_one_stack>,
	[[<ore:stickWood>,<ore:stickWood>,<ore:stickWood>],
	 [<minecraft:flint>,<storagedrawers:customdrawers:*>,<minecraft:flint>],
	 [<ore:stickWood>,<ore:stickWood>,<ore:stickWood>]]);


recipes.remove(<storagedrawers:customdrawers>);
recipes.addShaped("drawers_frame_0",<storagedrawers:customdrawers>,
	[[sidingPlankWood, <ore:stickWood>, sidingPlankWood],
	 [<ore:stickWood>, <ore:chestWood>, <ore:stickWood>],
	 [sidingPlankWood, <ore:stickWood>, sidingPlankWood]]);
recipes.remove(<storagedrawers:customdrawers:1>);
recipes.addShaped("drawers_frame_1",<storagedrawers:customdrawers:1>*2,
	[[sidingPlankWood, <ore:chestWood>, sidingPlankWood],
	 [<ore:stickWood>, <ore:stickWood>, <ore:stickWood>],
	 [sidingPlankWood, <ore:chestWood>, sidingPlankWood]]);
recipes.remove(<storagedrawers:customdrawers:2>);
recipes.addShaped("drawers_frame_2",<storagedrawers:customdrawers:2>*4,
	[[<ore:chestWood>, <ore:stickWood>, <ore:chestWood>],
	 [<ore:stickWood>, sidingPlankWood, <ore:stickWood>],
	 [<ore:chestWood>, <ore:stickWood>, <ore:chestWood>]]);
recipes.remove(<storagedrawers:customdrawers:3>);
recipes.addShaped("drawers_frame_3",<storagedrawers:customdrawers:3>*2,
	[[mouldingPlankWood, <ore:chestWood>, mouldingPlankWood],
	 [<ore:stickWood>, <ore:stickWood>, <ore:stickWood>],
	 [mouldingPlankWood, <ore:chestWood>, mouldingPlankWood]]);
recipes.remove(<storagedrawers:customdrawers:4>);
recipes.addShaped("drawers_frame_4",<storagedrawers:customdrawers:4>*4,
	[[<ore:chestWood>, <ore:stickWood>, <ore:chestWood>],
	 [<ore:stickWood>, mouldingPlankWood, <ore:stickWood>],
	 [<ore:chestWood>, <ore:stickWood>, <ore:chestWood>]]);
