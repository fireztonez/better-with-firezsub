<geolosys:ore>.addTooltip(format.yellow("Iron Ore"));
<geolosys:ore:1>.addTooltip(format.yellow("Iron and Nickel Ore"));
<geolosys:ore:2>.addTooltip(format.yellow("Copper Ore"));
<geolosys:ore:3>.addTooltip(format.yellow("Copper Ore"));
<geolosys:ore:4>.addTooltip(format.yellow("Tin Ore"));
<geolosys:ore:5>.addTooltip(format.yellow("Tin Ore"));
<geolosys:ore:6>.addTooltip(format.yellow("Silver and Lead Ore"));
<geolosys:ore:7>.addTooltip(format.yellow("Aluminum Ore"));
<geolosys:ore:8>.addTooltip(format.yellow("Platinum and Osmium Ore"));
<geolosys:ore:9>.addTooltip(format.yellow("Uranium Ore"));
<geolosys:ore:10>.addTooltip(format.yellow("Zinc Ore"));
<geolosys:ore_vanilla:1>.addTooltip(format.yellow("Redstone Ore"));
<geolosys:ore_vanilla:5>.addTooltip(format.yellow("Diamond Ore"));
<geolosys:ore_vanilla:6>.addTooltip(format.yellow("Emerald Ore"));

<quark:charcoal_block>.addTooltip(format.yellow("Infinite fire source"));



<minecraft:anvil>.addTooltip(format.red("The Deadweight cannot be used to apply enchantments"));
<immersiveengineering:metal_device0>.addTooltip(format.yellow("Capacity of 250,000 IF"));
<immersiveengineering:metal_device0:1>.addTooltip(format.yellow("Capacity of 2,500,000 IF"));
<immersiveengineering:metal_device0:2>.addTooltip(format.yellow("Capacity of 15,000,000 IF"));


<teslacorelib:energy_tier1>.addTooltip(format.yellow("Upgrades for Industrial Foregoing machines"));
<teslacorelib:energy_tier2>.addTooltip(format.yellow("Upgrades for Industrial Foregoing machines"));
<teslacorelib:speed_tier1>.addTooltip(format.yellow("Upgrades for Industrial Foregoing machines"));
<teslacorelib:speed_tier2>.addTooltip(format.yellow("Upgrades for Industrial Foregoing machines"));


<immersiveengineering:metal_device0:2>.addTooltip(format.yellow("Capacity of 15,000,000 IF"));

<immersiveengineering:metal_device0:2>.addTooltip(format.yellow("Capacity of 15,000,000 IF"));
