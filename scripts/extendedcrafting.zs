
recipes.addShapeless("extendedcrafting_component_basic",<extendedcrafting:material:14>,[<extendedcrafting:material:2>,<extendedcrafting:material:7>,<ore:stickIron>,<ore:stickIron>]);

recipes.addShapeless("extendedcrafting_component_advanced",<extendedcrafting:material:15>,[<extendedcrafting:material:2>,<extendedcrafting:material:7>,<ore:stickGold>,<ore:stickGold>]);

recipes.addShapeless("extendedcrafting_component_elite",<extendedcrafting:material:16>,[<extendedcrafting:material:2>,<extendedcrafting:material:7>,<ore:stick>,<ore:stick>]);

recipes.addShapeless("extendedcrafting_component_ultimate",<extendedcrafting:material:17>,[<extendedcrafting:material:2>,<extendedcrafting:material:7>,<ore:stickEnderium>,<ore:stickEnderium>]);

recipes.addShapeless("extendedcrafting_luminessence",<extendedcrafting:material:7>,[<astralsorcery:itemusabledust>,<astralsorcery:itemcraftingcomponent:2>,<minecraft:glowstone_dust>,<minecraft:gunpowder>]);
