import mods.immersiveengineering.MetalPress;
import crafttweaker.oredict.IOreDictEntry;
import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;

//Example:
//mods.immersiveengineering.MetalPress.addRecipe(IItemStack output, IIngredient input, IItemStack mold, int energy, @Optional int inputSize);

MetalPress.removeRecipe(<immersiveengineering:graphite_electrode>.withTag({graphDmg: 48000}));
MetalPress.addRecipe(<immersiveengineering:graphite_electrode>, <immersiveengineering:material:19>, <immersiveengineering:mold:2>, 750, 4);
MetalPress.addRecipe(<tconstruct:stone_stick>, <ore:stone>, <immersiveengineering:mold:2>, 650, 2);

MetalPress.addRecipe(<immersiveengineering:material:20>*2, <immersiveengineering:metal:30>, <immersiveengineering:mold:4>, 500, 1);
MetalPress.addRecipe(<immersiveengineering:material:21>*2, <immersiveengineering:metal:37>, <immersiveengineering:mold:4>, 500, 1);
MetalPress.addRecipe(<immersiveengineering:material:29>*2, <immersiveengineering:metal:38>, <immersiveengineering:mold:4>, 500, 1);

MetalPress.addRecipe(<minecraft:blaze_rod>, <minecraft:blaze_powder>, <immersiveengineering:mold:2>, 500, 4);
MetalPress.addRecipe(<primal:obsidian_plate>, <minecraft:obsidian>, <immersiveengineering:mold:0>, 1000, 1);
MetalPress.addRecipe(<primal:diamond_plate>, <betterwithmods:material:45>, <immersiveengineering:mold:0>, 1000, 1);

var compressionX9 as IItemStack[IIngredient] = {
		<ore:ingotIron> : <minecraft:iron_block>
};
