import mods.pneumaticcraft.thermopneumaticprocessingplant;

mods.pneumaticcraft.thermopneumaticprocessingplant.removeAllRecipes();

// Add a recipe converting an input fluid and item into an output fluid (item may be null)
//mods.pneumaticcraft.thermopneumaticprocessingplant.addRecipe(ILiquidStack liquidInput, IItemStack itemInput, double pressure, double temperature, ILiquidStack output);

mods.pneumaticcraft.thermopneumaticprocessingplant.addRecipe(<liquid:propene> * 1000, <betterwithmods:material:1>, 2.8, 473, <liquid:plastic> * 750);
