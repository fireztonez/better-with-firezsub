
var machineName = "catalytic_cracking_machine";


mods.modularmachinery.RecipeBuilder.newBuilder("catalytic_cracking_machine_liquidPlastic", machineName, 600)
	.addEnergyPerTickInput(10)
	.addFluidOutput(<liquid:propene> * 1500)
	.addFluidInput(<liquid:refined_oil> * 1500)
	.addFluidInput(<liquid:steam> * 1000)
	.build();
