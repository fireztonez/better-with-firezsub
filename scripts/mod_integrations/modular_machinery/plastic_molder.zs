var machineName = "plastic_molder";

mods.modularmachinery.RecipeBuilder.newBuilder("plastic_molder_plastic", machineName, 200)
	.addEnergyPerTickInput(5)
	.addItemOutput(<industrialforegoing:plastic> * 1)
  .addFluidInput(<liquid:plastic> * 500)
	.build();
