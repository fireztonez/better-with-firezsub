import mods.betterwithmods.Misc.setFurnaceSmeltingTime;
import crafttweaker.oredict.IOreDictEntry;


//mods.betterwithmods.Misc.setFurnaceSmeltingTime(IIngredient ingredient, int time)

mods.betterwithmods.Misc.setFurnaceSmeltingTime(<ore:oreIron>,1200);
mods.betterwithmods.Misc.setFurnaceSmeltingTime(<ore:oreCopper>,800);
mods.betterwithmods.Misc.setFurnaceSmeltingTime(<ore:oreSilver>,700);
mods.betterwithmods.Misc.setFurnaceSmeltingTime(<ore:oreGold>,800);
mods.betterwithmods.Misc.setFurnaceSmeltingTime(<ore:oreTin>,200);
mods.betterwithmods.Misc.setFurnaceSmeltingTime(<ore:oreAluminum>,600);
mods.betterwithmods.Misc.setFurnaceSmeltingTime(<ore:oreAluminum>,600);
mods.betterwithmods.Misc.setFurnaceSmeltingTime(<minecraft:cobblestone>,800);
mods.betterwithmods.Misc.setFurnaceSmeltingTime(<betterwithmods:cobblestone>,800);
mods.betterwithmods.Misc.setFurnaceSmeltingTime(<betterwithmods:cobblestone:1>,800);
mods.betterwithmods.Misc.setFurnaceSmeltingTime(<betterwithmods:cobblestone:2>,800);
mods.betterwithmods.Misc.setFurnaceSmeltingTime(<betterwithmods:cobblestone:2>,800);
mods.betterwithmods.Misc.setFurnaceSmeltingTime(<minecraft:stone:*>,800);
