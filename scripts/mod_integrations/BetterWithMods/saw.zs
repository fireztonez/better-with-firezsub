import mods.betterwithmods.Saw;
import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import mods.betterwithmods.MiniBlocks;

var strippedLogs = [<primal:logs_stripped>,<primal:logs_stripped:1>,<primal:logs_stripped:2>,<primal:logs_stripped:3>,<primal:logs_stripped:4>,<primal:logs_stripped:5>,<primal:logs_stripped:6>,<primal:logs_stripped:7>,<primal:corypha_stalk:1>] as IItemStack[];

var planks = [<minecraft:planks>,<minecraft:planks:1>,<minecraft:planks:2>,<minecraft:planks:3>,<minecraft:planks:4>,<minecraft:planks:5>,<primal:planks>,<primal:planks:1>,<primal:planks:3>] as IItemStack[];

for i, blocks in strippedLogs {
	Saw.remove(blocks);
	Saw.add(blocks, [planks[i]*4, <betterwithmods:material:22>]);
}

//Saw.remove([<betterwithmods:siding_wood>.withTag({texture: {Properties: {type: "ironwood"}, Name: "primal:planks"}})]);
//Saw.remove([<betterwithmods:moulding_wood>.withTag({texture: {Properties: {type: "ironwood"}, Name: "primal:planks"}})]);
//Saw.remove([<betterwithmods:corner_wood>.withTag({texture: {Properties: {type: "ironwood"}, Name: "primal:planks"}})]);
