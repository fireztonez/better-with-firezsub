import mods.betterwithmods.Kiln;
//Take notes: Kiln recipes is always stoked, unless using Kiln Builder to specified unstocked...

//var allBricks = [<betterwithaddons:bricks_stained:9>,<betterwithaddons:bricks_stained:10>,<betterwithaddons:bricks_stained:11>,<betterwithaddons:bricks_stained:12>,<betterwithaddons:bricks_stained:13>,<betterwithaddons:bricks_stained:14>,<betterwithaddons:bricks_stained:2>,<betterwithaddons:bricks_stained>,<betterwithaddons:bricks_stained:1>,<betterwithaddons:bricks_stained:15>,<betterwithaddons:bricks_stained:3>,<betterwithaddons:bricks_stained:4>,<betterwithaddons:bricks_stained:5>,<betterwithaddons:bricks_stained:6>,<betterwithaddons:bricks_stained:7>,<betterwithaddons:bricks_stained:8>,<ceramics:clay_hard:1>,<ceramics:clay_hard:2>,<ceramics:clay_hard:3>,<ceramics:clay_hard:4>,<ceramics:clay_hard:5>,<ceramics:clay_hard:6>,<chisel:bricks>,<chisel:bricks:1>,<chisel:bricks:2>,<chisel:bricks:3>,<chisel:bricks:4>,<chisel:bricks:5>,<chisel:bricks:6>,<chisel:bricks:7>,<chisel:bricks:8>,<chisel:bricks:9>,<chisel:bricks:10>,<chisel:bricks:11>,<chisel:bricks:12>,<chisel:bricks:13>,<chisel:bricks:14>,<chisel:bricks:15>,<chisel:bricks1>,<chisel:bricks1:1>,<chisel:bricks1:2>,<chisel:bricks1:3>,<chisel:bricks1:4>,<chisel:bricks1:5>,<chisel:bricks1:6>,<chisel:bricks1:7>,<chisel:bricks1:8>,<chisel:bricks1:9>,<chisel:bricks2>,<chisel:bricks2:1>,<chisel:bricks2:2>,<chisel:bricks2:3>,<chisel:bricks2:4>,<chisel:bricks2:5>,<primal:adobebrick>] as IItemStack[];
var bwaBricks = <betterwithaddons:bricks_stained>.definition;
var chiselBricks = <chisel:bricks>.definition;
var chiselBricks1 = <chisel:bricks1>.definition;
var chiselBricks2 = <chisel:bricks2>.definition;
var ceramicsBricks2 = <ceramics:clay_hard>.definition;

for meta in 0 to 15 {
	mods.betterwithmods.Kiln.registerBlock(bwaBricks.makeStack(meta));
	mods.betterwithmods.Kiln.registerBlock(chiselBricks.makeStack(meta));
}

for meta in 0 to 9 {
	mods.betterwithmods.Kiln.registerBlock(chiselBricks1.makeStack(meta));
}
for meta in 0 to 5 {
	mods.betterwithmods.Kiln.registerBlock(chiselBricks2.makeStack(meta));
}
for meta in 0 to 6 {
	mods.betterwithmods.Kiln.registerBlock(ceramicsBricks2.makeStack(meta));
}
mods.betterwithmods.Kiln.registerBlock(<primal:adobe_brick>);

furnace.remove(<tconstruct:materials>);
Kiln.add(<tconstruct:soil>, [<tconstruct:materials>]);

Kiln.add(<astralsorcery:blockcustomore:1>, [<astralsorcery:itemcraftingcomponent:1>]);

Kiln.add(<abyssalcraft:coraliumstone>, [<abyssalcraft:cpearl>]);
