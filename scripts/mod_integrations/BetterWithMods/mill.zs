import mods.betterwithmods.Mill;


Mill.addRecipe([<ore:barkWood>], [<primal:tannin_ground>]);
Mill.addRecipe([<primal:mud_dried>], [<minecraft:dirt>]);//Sulfur Crystal to Sulfur Dust
Mill.addRecipe([<minecraft:quartz>], [<actuallyadditions:item_dust:5>]);//Quartz dust
Mill.addRecipe([<ore:cobblestone>], [<minecraft:gravel>]);//Quartz dust

recipes.remove(<ceramics:unfired_clay:4>);
Mill.add(<ceramics:unfired_clay:4>*2, null, [<minecraft:clay_ball>*2,<minecraft:flint>,<ore:dustQuartz>]);#Porcelain recipe

recipes.remove(<tconstruct:soil>);
Mill.add(<tconstruct:soil>*4, null, [<ceramics:clay_soft>, <ore:sand>*4, <minecraft:gravel>*4]);//Grout recipes

recipes.remove(<primal:adobe_clump>);
Mill.add(<primal:adobe_clump>, [<primal:mud_clump>,<ore:thatchingDry>]);

Mill.add(<darkutils:material>, [<witherskelefix:fragment>]); // Wither Dust


Mill.add(<ceramics:unfired_clay:4> * 6, [<ceramics:clay_barrel_unfired:2> | <ceramics:clay_barrel_unfired:3> | <ceramics:unfired_clay:6> | <ceramics:unfired_clay:7>]); // Uncrafting Unfired porcelain barrel

Mill.add(<minecraft:ender_pearl>, [<appliedenergistics2:material:46>]);
