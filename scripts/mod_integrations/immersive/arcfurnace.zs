import mods.immersiveengineering.ArcFurnace;


//mods.immersiveengineering.ArcFurnace.addRecipe(IItemStack output, IIngredient input, IItemStack slag, int time, int energyPerTick, @Optional IIngredient[] additives, @Optional String specialRecipeType);

ArcFurnace.addRecipe(<extendedcrafting:material>*3, <darkutils:material>, slag, 200, 512, [<minecraft:iron_ingot>*2]);


ArcFurnace.addRecipe(<nuclearcraft:part>*4, <nuclearcraft:ingot_block:8>, slag, 80, 2048, [<immersiveengineering:metal:32>*4, <betterwithmods:material:14>*4]);
ArcFurnace.addRecipe(<nuclearcraft:part:1>, <nuclearcraft:part>, slag, 80, 2048, [<nuclearcraft:alloy:1>*4, <minecraft:redstone>*4]);
ArcFurnace.addRecipe(<nuclearcraft:part:2>, <nuclearcraft:part:1>, slag, 80, 2048, [<nuclearcraft:uranium:9>*4, <ore:dustSulfur>*4]);
ArcFurnace.addRecipe(<nuclearcraft:part:3>, <nuclearcraft:part:2>, slag, 80, 2048, [<ore:ingotBoron>*4, <ore:dustCrystalBinder>*4]);
