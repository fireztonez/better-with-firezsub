import mods.immersiveengineering.MetalPress;

//Example:
//mods.immersiveengineering.MetalPress.addRecipe(IItemStack output, IIngredient input, IItemStack mold, int energy, @Optional int inputSize);
//
//mods.immersiveengineering.MetalPress.addRecipe(<minecraft:diamond>, <ore:logWood>, <minecraft:emerald>, 2000);
//mods.immersiveengineering.MetalPress.addRecipe(<minecraft:diamond>, <ore:logWood>, <minecraft:emerald>, 2000, 16);

//mods.immersiveengineering.MetalPress.addRecipe(<extendedcrafting:material:2>, <extendedcrafting:material>, metalItems.iron.ingot, 2000);
//mods.immersiveengineering.MetalPress.addRecipe(<extendedcrafting:material:2>, <extendedcrafting:material>, metalItems.iron.ingot, 2000);
//mods.immersiveengineering.MetalPress.addRecipe(<extendedcrafting:material:2>, <extendedcrafting:material>, metalItems.iron.ingot, 2000);

//Plate
MetalPress.addRecipe(metalItems.modularium.plate, metalItems.modularium.ingot,<immersiveengineering:mold:2>, 2000, 1);
MetalPress.addRecipe(metalItems.soulforgedSteel.plate, metalItems.soulforgedSteel.ingot,<immersiveengineering:mold:2>, 2000, 1);

//Gears
MetalPress.addRecipe(metalItems.modularium.gear, metalItems.modularium.ingot,<immersiveengineering:mold:2>, 2000, 4);

//Rod
MetalPress.addRecipe(metalItems.modularium.rod * 2, metalItems.modularium.ingot,<immersiveengineering:mold:2>, 2000, 1);

//Unpackager
MetalPress.addRecipe(metalItems.modularium.nugget * 9, metalItems.modularium.ingot,<immersiveengineering:mold:7>, 2000, 1);
MetalPress.addRecipe(metalItems.modularium.ingot * 9, metalItems.modularium.block,<immersiveengineering:mold:7>, 2000, 1);
