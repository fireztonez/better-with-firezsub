import mods.immersiveengineering.AlloySmelter;

recipes.remove(<extendedcrafting:material>);
AlloySmelter.addRecipe(<extendedcrafting:material>*3
, <ore:ingotIron>*2, <darkutils:material>, 2000);
