import mods.immersivepetroleum.Distillation;
import mods.immersivepetroleum.Motorboat;
import mods.immersivepetroleum.PortableGenerator;
import mods.immersivepetroleum.Reservoir;

/*
	Distillation
	http://crafttweaker.readthedocs.io/en/latest/#Mods/Immersive_Petroleum/CraftTweaker_Support/Distillation/
*/

Distillation.addRecipe(
	[<liquid:diesel> * 35, <liquid:gasoline> * 65],
	[<immersivepetroleum:material>],
	<liquid:crude_oil> * 100,
	2048, 1,
	[0.035]
);

Distillation.addRecipe(
	[<liquid:refined_oil> * 60, <liquid:lubricant> * 30, <liquid:kerosene> * 10],
	[<immersivepetroleum:material>],
	<liquid:gasoline> * 100,
	2048, 1,
	[0.035]
);

// List of reservoir types. Format: name, fluid_name, min_mb_fluid, max_mb_fluid, mb_per_tick_replenish, weight, [dim_blacklist], [dim_whitelist], [biome_dict_blacklist], [biome_dict_whitelist]
//Reservoir.registerReservoir(String name, ILiquidStack fluid, int minSize, int maxSize, int replenishRate, int weight);
Reservoir.registerReservoir("Aquifer", <liquid:water>, 5000000, 10000000, 6, 30, [1,-1], [0], [], []);
Reservoir.registerReservoir("Lava", <liquid:lava>, 250000, 1000000, 0, 12, [1], [], [], []);
Reservoir.registerReservoir("Magma Basaltic", <liquid:magma_basaltic>, 250000, 1000000, 0, 4, [1], [], [], []);
Reservoir.registerReservoir("Crude Oil", <liquid:crude_oil>, 100000, 800000, 5, 8, [1], [], [], [] );


Motorboat.registerMotorboatFuel(<liquid:gasoline>, 4);
Motorboat.registerMotorboatFuel(<liquid:kerosene>, 1);
PortableGenerator.registerPortableGenFuel(<liquid:gasoline>, 256, 5);
PortableGenerator.registerPortableGenFuel(<liquid:kerosene>, 512, 2);
