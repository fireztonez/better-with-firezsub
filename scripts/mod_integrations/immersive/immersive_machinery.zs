import mods.immersiveengineering.Mixer;
import mods.immersiveengineering.Squeezer;
import mods.immersiveengineering.Crusher;



Squeezer.removeItemRecipe(<minecraft:leather>); //Remove Rotten Flesh -> Leather

//mods.immersiveengineering.Mixer.addRecipe(ILiquidStack output, ILiquidStack fluidInput, IIngredient[] itemInputs, int energy);
//mods.immersiveengineering.Mixer.addRecipe(<liquid:lava>, <liquid:water>, [<ore:logWood>, <minecraft:dirt>], 2048);

//*****************************************************************
//CRUSHER RECIPES
//mods.immersiveengineering.Crusher.addRecipe(IItemStack output, IIngredient input, int energy, @Optional IItemStack secondaryOutput, @Optional double secondaryChance);
//*****************************************************************
Crusher.removeRecipe(<primal:platinum_dust>);
Crusher.removeRecipe(<nuclearcraft:gem_dust:2>);
Crusher.addRecipe(<primal:platinum_dust> * 2, <ore:orePlatinum>, 6000, <contenttweaker:material_part:35>, 0.05);

Crusher.addRecipe(<betterwithmods:material:18>, <minecraft:coal>, 3000);
Crusher.addRecipe(<betterwithmods:material:37>, <minecraft:coal:1>, 3000);

Crusher.addRecipe(<actuallyadditions:item_dust:5>, <minecraft:quartz>, 3000);



//<forge:bucketfilled>.withTag({FluidName: "potion", Amount: 1000, Tag: {Potion: "minecraft:healing"}})
//<minecraft:potion>.withTag({Potion: "minecraft:poison"})
//<minecraft:potion>.withTag({Potion: "minecraft:long_poison"})
//<minecraft:potion>.withTag({Potion: "minecraft:night_vision"})
//<minecraft:potion>.withTag({Potion: "minecraft:invisibility"})
//<minecraft:potion>.withTag({Potion: "minecraft:long_invisibility"})
//<minecraft:potion>.withTag({Potion: "minecraft:long_fire_resistance"})
//<minecraft:potion>.withTag({Potion: "minecraft:water_breathing"})
//<minecraft:potion>.withTag({Potion: "minecraft:long_water_breathing"})
//<minecraft:potion>.withTag({Potion: "minecraft:harming"})
//<minecraft:potion>.withTag({Potion: "minecraft:strong_harming"})
//<minecraft:potion>.withTag({Potion: "minecraft:strong_poison"})
//<minecraft:potion>.withTag({Potion: "minecraft:regeneration"})
//<minecraft:potion>.withTag({Potion: "minecraft:long_regeneration"})
//<minecraft:potion>.withTag({Potion: "minecraft:strong_regeneration"})
//<minecraft:potion>.withTag({Potion: "minecraft:strength"})
//<minecraft:potion>.withTag({Potion: "minecraft:long_strength"})
//<minecraft:potion>.withTag({Potion: "minecraft:strong_strength"})
//<minecraft:potion>.withTag({Potion: "minecraft:weakness"})
//<minecraft:potion>.withTag({Potion: "minecraft:long_weakness"})
