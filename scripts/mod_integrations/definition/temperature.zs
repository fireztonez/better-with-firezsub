<liquid:magma_basaltic>.definition.temperature = 3500;
<liquid:pyrotheum>.definition.temperature = 5000;

mods.tconstruct.Fuel.registerFuel(<liquid:pyrotheum> * 25, 400);
mods.tconstruct.Fuel.registerFuel(<liquid:pyrotheum> * 25, 600);
