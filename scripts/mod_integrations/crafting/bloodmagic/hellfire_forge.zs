import mods.bloodmagic.TartaricForge;

//mods.bloodmagic.TartaricForge.addRecipe(IItemStack output, IItemStack[] inputs, double minSouls, double soulDrain);

TartaricForge.addRecipe(
    <astralsorcery:itemcraftingcomponent:2>,
    [<immersiveengineering:metal:18>, <minecraft:dye:4>, <appliedenergistics2:material:46>, <minecraft:gunpowder>],
    80, 20
);
