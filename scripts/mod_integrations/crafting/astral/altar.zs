import mods.astralsorcery.Altar;

# https://crafttweaker.readthedocs.io/en/latest/#Mods/Astral_Sorcery/Altar/

#  Altar Levels :
#0	Luminous Crafting Table
#1	Starlight Crafting Altar
#2	Celestial Altar

#Altar.addAttunmentAltarRecipe(<minecraft:dirt>, 500, 300, [
#    null, null, null,
#    <ore:treeLeaves>, <astralsorcery:blockmarble:2>, <ore:treeLeaves>,
#    null, <liquid:astralsorcery.liquidstarlight>, null,
#    <ore:blockMarble>, <ore:blockMarble>, <ore:blockMarble>, <ore:blockMarble>]);


recipes.remove(<tconstruct:tooltables:1>);
mods.astralsorcery.Altar.addAttunementAltarRecipe("attunement_stencilStation", <tconstruct:tooltables:1>.withTag({textureBlock: {id: "chisel:planks-acacia", Count: 1 as byte, Damage: 3 as short}}), 1200, 300, [
    <astralsorcery:blockinfusedwood:3>, <tconstruct:pattern>, <astralsorcery:blockinfusedwood:3>,
    <minecraft:planks>, <tconstruct:tooltables>, <minecraft:planks>,
    <minecraft:planks>, null, <minecraft:planks>,
    <tconstruct:pattern>, <tconstruct:pattern>, null, null]);

recipes.remove(<tconstruct:tooltables:2>);
mods.astralsorcery.Altar.addAttunementAltarRecipe("attunement_partBuilder", <tconstruct:tooltables:2>.withTag({textureBlock: {id: "minecraft:log", Count: 1 as byte, Damage: 0 as short}}), 500, 300, [
    <astralsorcery:blockinfusedwood:1>, <astralsorcery:blockinfusedwood:1>, <astralsorcery:blockinfusedwood:1>,
    <minecraft:log>, <tconstruct:tooltables>, <minecraft:log>,
    <minecraft:log>, null, <minecraft:log>,
    <tconstruct:pattern>, <tconstruct:pattern>, <tconstruct:pattern>, <tconstruct:pattern>]);

recipes.remove(<tconstruct:tooltables:3>);
mods.astralsorcery.Altar.addAttunementAltarRecipe("attunement_toolStation", <tconstruct:tooltables:3>, 1200, 300, [
    <tconstruct:seared:3>, <tconstruct:seared:3>, <tconstruct:seared:3>,
    <astralsorcery:blockinfusedwood:1>, <tconstruct:tooltables>, <astralsorcery:blockinfusedwood:1>,
    <astralsorcery:blockinfusedwood:1>, null, <astralsorcery:blockinfusedwood:1>,
    <minecraft:bow>, <minecraft:iron_sword>, <minecraft:iron_pickaxe>, <minecraft:iron_axe>]);

recipes.remove(<tconstruct:tooltables:4>);
mods.astralsorcery.Altar.addAttunementAltarRecipe("attunement_patternChest", <tconstruct:tooltables:4>, 1200, 300, [
    sidingPlankWood, sidingPlankWood, sidingPlankWood,
    null, <ore:chestWood>, null,
    sidingPlankWood, sidingPlankWood, sidingPlankWood,
    <tconstruct:pattern>, <tconstruct:pattern>, <tconstruct:pattern>, <tconstruct:pattern>]);

recipes.remove(<tconstruct:tooltables:5>);
mods.astralsorcery.Altar.addAttunementAltarRecipe("attunement_partChest", <tconstruct:tooltables:5>, 1200, 300, [
    sidingPlankWood, sidingPlankWood, sidingPlankWood,
    null, <ore:chestWood>, null,
    sidingPlankWood, sidingPlankWood, sidingPlankWood,
    <ore:stickWood>, <ore:stickWood>, <ore:stickWood>, <ore:stickWood>]);


//Crafting Altar
Altar.addAttunementAltarRecipe("attunement_oreStarMetal", <astralsorcery:blockcustomore:1>, 200, 1600, [
    null, <astralsorcery:itemcraftingcomponent:2>, null,
    <minecraft:stone>, <minecraft:flint>, <minecraft:stone>,
    null, <astralsorcery:itemcraftingcomponent:2>, null,
    <astralsorcery:itemusabledust>, <astralsorcery:itemusabledust>, <astralsorcery:itemusabledust:1>, <astralsorcery:itemusabledust:1>]);
