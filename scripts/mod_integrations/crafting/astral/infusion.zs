import mods.astralsorcery.Altar;

//mods.astralsorcery.StarlightInfusion.addInfusion(IItemStack input, IItemStack output, boolean consumeMultiple, float consumptionChance, int craftingTickTime);
mods.astralsorcery.StarlightInfusion.addInfusion(<tconstruct:tooltables:3>, <tconstruct:toolforge>.withTag({textureBlock: {id: "chisel:marble2", Count: 1 as byte, Damage: 5 as short}}), true, 1, 500);

mods.astralsorcery.StarlightInfusion.addInfusion(metalItems.osmium.dust, <astralsorcery:itemcraftingcomponent:2>, false, 0.5, 200);

mods.astralsorcery.StarlightInfusion.addInfusion(<betterwithmods:material:17>, <minecraft:nether_wart>, false, 1, 200);
