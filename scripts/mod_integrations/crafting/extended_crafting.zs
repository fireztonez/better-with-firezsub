

mods.extendedcrafting.TableCrafting.addShaped(0, <compactmachines3:fieldprojector> * 4, [
	[null, null, null, <ore:ingotBlackIron>, <ore:netherStar>],
	[null, null, <cyclicmagic:laser>, <ore:ingotBlackIron>, <industrialforegoing:laser_lens:1>],
	[null, null, <pneumaticcraft:assembly_laser>, <ore:ingotBlackIron>, <ore:netherStar>],
	[<industrialforegoing:plastic>, <ore:ingotDiamond>, <ore:ingotDiamond>, <ore:ingotDiamond>, <industrialforegoing:plastic>],
	[<extendedcrafting:trimmed:2>, <ore:ingotBlackIron>, <ore:ingotBlackIron>, <ore:ingotBlackIron>, <extendedcrafting:trimmed:2>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <oeintegration:excavatemodifier>*27, [
	[null, <ore:platePlatinum>, <ore:slimecrystal>, <ore:platePlatinum>, null],
	[<ore:platePlatinum>, <ore:itemRubber>, <ore:ingotLumium>, <ore:itemRubber>, <ore:platePlatinum>],
	[<ore:slimecrystal>, <ore:ingotLumium>, <ore:coinLumium>, <ore:coinLumium>, <ore:slimecrystal>],
	[<ore:platePlatinum>, <ore:itemRubber>, <ore:coinLumium>, <ore:itemRubber>, <ore:platePlatinum>],
	[null, <ore:platePlatinum>, <ore:slimecrystal>, <ore:platePlatinum>, null]
]);


mods.extendedcrafting.TableCrafting.addShaped(0, <cyclicmagic:glowing_helmet>, [
    [<bloodmagic:component:7>, <minecraft:potion>.withTag({Potion: "minecraft:long_night_vision"}), <astralsorcery:blockworldilluminator>],
    [<extendedcrafting:lamp:1>, <minecraft:iron_helmet>, <extendedcrafting:lamp:1>],
    [null, null, null]
]);

recipes.remove(<nuclearcraft:part:10>);
mods.extendedcrafting.TableCrafting.addShaped(2, <nuclearcraft:part:10>, [
	[<ore:blockSheetmetalLead>, <industrialforegoing:plastic>, <ore:plateSteel>, <industrialforegoing:plastic>, <ore:blockSheetmetalLead>],
	[<industrialforegoing:plastic>, <ore:ingotIronCompressed>, <pneumaticcraft:transistor>, <ore:ingotIronCompressed>, <industrialforegoing:plastic>],
	[<ore:plateSteel>, <pneumaticcraft:printed_circuit_board>, <ore:solenoidMagnesiumDiboride>, <pneumaticcraft:printed_circuit_board>, <ore:plateSteel>],
	[<industrialforegoing:plastic>, <ore:ingotIronCompressed>, <pneumaticcraft:capacitor>, <ore:ingotIronCompressed>, <industrialforegoing:plastic>],
	[<ore:blockSheetmetalLead>, <industrialforegoing:plastic>, <ore:plateSteel>, <industrialforegoing:plastic>, <ore:blockSheetmetalLead>]
]);

recipes.remove(<nuclearcraft:part:12>);
mods.extendedcrafting.TableCrafting.addShaped(2, <nuclearcraft:part:12>, [
	[<ore:blockSheetmetalSteel>, <ore:bioplastic>, <ore:plasticGray>, <ore:bioplastic>, <ore:blockSheetmetalSteel>],
	[<ore:bioplastic>, <ore:ingotTough>, <ore:plateSoulforgedSteel>, <ore:ingotTough>, <ore:bioplastic>],
	[<ore:plasticGray>, <ore:plateSoulforgedSteel>, <pneumaticcraft:printed_circuit_board>, <ore:plateSoulforgedSteel>, <ore:plasticGray>],
	[<ore:bioplastic>, <ore:ingotTough>, <ore:plateSoulforgedSteel>, <ore:ingotTough>, <ore:bioplastic>],
	[<ore:blockSheetmetalSteel>, <ore:bioplastic>, <ore:plasticGray>, <ore:bioplastic>, <ore:blockSheetmetalSteel>]
]);
