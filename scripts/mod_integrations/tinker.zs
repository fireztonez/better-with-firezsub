import crafttweaker.item.IItemDefinition;
import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import crafttweaker.liquid.ILiquidStack;

import mods.tconstruct.Melting;
import mods.tconstruct.Casting;
import mods.tconstruct.Alloy;

mods.tconstruct.Melting.removeRecipe(<liquid:iron>, <minecraft:anvil>);
mods.tconstruct.Melting.removeRecipe(<liquid:iron>, <minecraft:iron_bars>);
mods.tconstruct.Melting.removeRecipe(<liquid:iron>, <minecraft:iron_trapdoor>);
mods.tconstruct.Melting.removeRecipe(<liquid:iron>, <minecraft:iron_door>);

//<contenttweaker:material_part:80>
//<contenttweaker:material_part:81>
//<contenttweaker:material_part:82>
//<contenttweaker:material_part:83>
//<contenttweaker:sub_block_holder_1:8>
//<contenttweaker:sub_block_holder_0:11>
//<geolosys:cluster:12>
//



Melting.removeEntityMelting(<entity:minecraft:villager>);
Melting.removeRecipe(<liquid:stone>);
Melting.removeRecipe(<liquid:coal>);
Melting.removeRecipe(<liquid:diamond>);

Melting.addRecipe(<liquid:stone> * 288,<tconstruct:seared:*>);
Melting.addRecipe(<liquid:stone> * 72,<tconstruct:materials>);

Melting.addRecipe(<liquid:glass> * 250, <ore:shardGlass>);

Melting.addRecipe(<liquid:ender> * 250, <minecraft:ender_pearl>);
Melting.addRecipe(<liquid:ender> * 250, <appliedenergistics2:material:46>);
Melting.addRecipe(<liquid:ender> * 250, <contenttweaker:broken_enderpearl>);

Melting.addRecipe(<liquid:stone> * 288,<tconstruct:seared:*>);
Melting.addRecipe(<liquid:stone> * 72,<tconstruct:materials>);

Melting.addRecipe(<liquid:redstone>*100, <minecraft:redstone>);
Melting.addRecipe(<liquid:redstone>*900, <minecraft:redstone_block>);

Melting.addRecipe(<liquid:glowstone>*100, <minecraft:glowstone_dust>);
Melting.addRecipe(<liquid:glowstone>*400, <ore:glowstone>);

Melting.addRecipe(<liquid:redstone_alloy>*1296, metalItems.redstoneAlloy.block);
Melting.addRecipe(<liquid:redstone_alloy>*144, metalItems.redstoneAlloy.plate);
Melting.addRecipe(<liquid:redstone_alloy>*144, metalItems.redstoneAlloy.ingot);
Melting.addRecipe(<liquid:redstone_alloy>*576, metalItems.redstoneAlloy.gear);
Melting.addRecipe(<liquid:redstone_alloy>*72, metalItems.redstoneAlloy.rod);
Melting.addRecipe(<liquid:redstone_alloy>*16, metalItems.redstoneAlloy.nugget);

Melting.addRecipe(<liquid:osmium>*144, <ore:oreOsmium>);
Melting.addRecipe(<liquid:osmium>*1296, metalItems.osmium.block);
Melting.addRecipe(<liquid:osmium>*144, metalItems.osmium.dust);
Melting.addRecipe(<liquid:osmium>*144, metalItems.osmium.ingot);
Melting.addRecipe(<liquid:osmium>*16, metalItems.osmium.nugget);
Melting.addRecipe(<liquid:osmium>*144, metalItems.osmium.plate);

Melting.addRecipe(<liquid:osmium>*144, <ore:oreOsmium>);

Melting.addRecipe(<liquid:invar>*72, metalItems.invar.rod);

//-------
// ALLOY

var alloyLiquid as ILiquidStack[] = [
    <liquid:steel>,
    <liquid:hard_carbon>,
    <liquid:tough>,
    <liquid:enderium>,
    <liquid:ferroboron>
];
for alloy in alloyLiquid {
    Alloy.removeRecipe(alloy);
}

Alloy.addRecipe(<liquid:invar>*3, [<liquid:iron>*2, <liquid:nickel>*1]);

Alloy.addRecipe(<liquid:redstone_alloy>*144, [<liquid:invar>*144, <liquid:redstone>*100, <liquid:glowstone>*100]);

Alloy.addRecipe(<liquid:modularium>*288, [<liquid:gold> * 144, <liquid:aluminum> * 144, <liquid:redstone> * 200]);

//CASTING
Casting.removeBasinRecipe(<tconstruct:firewood>);
Casting.addBasinRecipe(<tconstruct:firewood>, <ore:plankWood>, <liquid:lava>, 500);

Casting.addTableRecipe(<tconstruct:cast_custom:2>, <ore:castGems>, <liquid:gold>, 288, true);
Casting.addTableRecipe(<tconstruct:cast_custom:2>, <ore:castGems>, <liquid:alubrass>, 144, true);
Casting.addTableRecipe(<tconstruct:cast_custom:2>, <ore:castGems>, <liquid:brass>, 144, true);

Casting.addTableRecipe(<minecraft:ender_pearl>, <tconstruct:cast>.withTag({PartType: "tconstruct:pan_head"}), <liquid:ender>, 250, false);

Casting.addTableRecipe(<minecraft:redstone>, <tconstruct:cast_custom:2>, <liquid:redstone>, 100);

Casting.addBasinRecipe(metalItems.modularium.block, null, <liquid:modularium>, 1296);
Casting.addTableRecipe(metalItems.modularium.ingot, <tconstruct:cast_custom>, <liquid:modularium>, 144);
Casting.addTableRecipe(metalItems.modularium.plate, <tconstruct:cast_custom:3>, <liquid:modularium>, 288);
Casting.addTableRecipe(metalItems.modularium.nugget, <tconstruct:cast_custom:1>, <liquid:modularium>, 16);
Casting.addTableRecipe(metalItems.modularium.gear, <tconstruct:cast_custom:4>, <liquid:modularium>, 576);

Casting.addBasinRecipe(metalItems.osmium.block, null, <liquid:osmium>, 1296);
Casting.addTableRecipe(metalItems.osmium.ingot, <tconstruct:cast_custom>, <liquid:osmium>, 144);
Casting.addTableRecipe(metalItems.osmium.nugget, <tconstruct:cast_custom:1>, <liquid:osmium>, 16);
Casting.addTableRecipe(metalItems.osmium.plate, <tconstruct:cast_custom:3>, <liquid:osmium>, 288);

Casting.addBasinRecipe(<minecraft:redstone_block>, null, <liquid:redstone>, 900);

Casting.addTableRecipe(<minecraft:redstone>, <tconstruct:cast_custom:2>, <liquid:redstone>, 100);
Casting.addBasinRecipe(<minecraft:redstone_block>, null, <liquid:redstone>, 900);

Casting.addTableRecipe(<minecraft:glowstone_dust>, <tconstruct:cast_custom:2>, <liquid:glowstone>, 100);
Casting.addBasinRecipe(<minecraft:glowstone>, null, <liquid:glowstone>, 400);
#Invar
#Casting.addTableRecipe(<contenttweaker:, <tconstruct:cast_custom>, <liquid:invar>, 144);
#Casting.addBasinRecipe(<contenttweaker:, null, <liquid:invar>, 1296);

Casting.addBasinRecipe(metalItems.redstoneAlloy.block, null, <liquid:redstone_alloy>, 1296);
Casting.addTableRecipe(metalItems.redstoneAlloy.ingot, <tconstruct:cast_custom>, <liquid:redstone_alloy>, 144);
Casting.addTableRecipe(metalItems.redstoneAlloy.plate, <tconstruct:cast_custom:3>, <liquid:redstone_alloy>, 288);
Casting.addTableRecipe(metalItems.redstoneAlloy.nugget, <tconstruct:cast_custom:1>, <liquid:redstone_alloy>, 16);
Casting.addTableRecipe(metalItems.redstoneAlloy.gear, <tconstruct:cast_custom:4>, <liquid:redstone_alloy>, 576);


Casting.addBasinRecipe(metalItems.invar.block, null, <liquid:invar>, 1296);
Casting.addTableRecipe(metalItems.invar.ingot, <tconstruct:cast_custom>, <liquid:invar>, 144);
Casting.addTableRecipe(metalItems.invar.plate, <tconstruct:cast_custom:3>, <liquid:invar>, 288);
Casting.addTableRecipe(metalItems.invar.nugget, <tconstruct:cast_custom:1>, <liquid:invar>, 16);
Casting.addTableRecipe(metalItems.invar.gear, <tconstruct:cast_custom:4>, <liquid:invar>, 576);
