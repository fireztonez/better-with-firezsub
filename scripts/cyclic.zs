
recipes.remove(<cyclicmagic:item_pipe>);
recipes.addShaped("cyclic_pipe_item", <cyclicmagic:item_pipe>*8,
	[[<minecraft:nether_brick_stairs>,<immersiveengineering:metal:32>,<minecraft:nether_brick_stairs>],
	[<immersiveengineering:metal:32>,<immersiveengineering:conveyor>,<immersiveengineering:metal:32>],
	[<minecraft:nether_brick_stairs>,<immersiveengineering:metal:32>,<minecraft:nether_brick_stairs>]]);
recipes.remove(<cyclicmagic:item_pump>);
recipes.addShaped("cyclic_pump_item", <cyclicmagic:item_pump>,
	[[<minecraft:hopper>],
	[<cyclicmagic:item_pipe>],
	[<betterwithmods:material:34>]]);
recipes.remove(<cyclicmagic:fluid_pipe>);
recipes.addShaped("cyclic_pipe_fluid", <cyclicmagic:fluid_pipe>*8,
	[[<minecraft:nether_brick_stairs>,<immersiveengineering:metal:33>,<minecraft:nether_brick_stairs>],
	[<immersiveengineering:metal:33>,<minecraft:bucket>,<immersiveengineering:metal:33>],
	[<minecraft:nether_brick_stairs>,<immersiveengineering:metal:33>,<minecraft:nether_brick_stairs>]]);
recipes.remove(<cyclicmagic:fluid_pump>);
recipes.addShaped("cyclic_pump_fluid", <cyclicmagic:fluid_pump>,
	[[<minecraft:hopper>],
	[<cyclicmagic:fluid_pipe>],
	[<betterwithmods:material:34>]]);
recipes.remove(<cyclicmagic:energy_pipe>);
recipes.addShaped("cyclic_pipe_energy", <cyclicmagic:energy_pipe>*8,
	[[<minecraft:nether_brick_stairs>,<immersiveengineering:metal:37>,<minecraft:nether_brick_stairs>],
	[<immersiveengineering:metal:37>,<minecraft:redstone_block>,<immersiveengineering:metal:37>],
	[<minecraft:nether_brick_stairs>,<immersiveengineering:metal:37>,<minecraft:nether_brick_stairs>]]);
recipes.remove(<cyclicmagic:energy_pump>);
recipes.addShaped("cyclic_pump_energy", <cyclicmagic:energy_pump>,
	[[<minecraft:hopper>],
	[<cyclicmagic:energy_pipe>],
	[<betterwithmods:material:34>]]);


recipes.remove(<cyclicmagic:bundled_pipe>);
recipes.addShapedMirrored("cyclic_pipe_bundled", <cyclicmagic:bundled_pipe>*8,
	[[moulding_soulforgedsteel,<immersiveengineering:stone_decoration:3>,moulding_soulforgedsteel],
	[<cyclicmagic:item_pipe>,<cyclicmagic:energy_pipe>,<cyclicmagic:fluid_pipe>],
	[moulding_soulforgedsteel,<immersiveengineering:stone_decoration:3>,moulding_soulforgedsteel]]);

recipes.remove(<cyclicmagic:horse_upgrade_health>);
recipes.addShaped("cyclic_horse_upgrade_health", <cyclicmagic:horse_upgrade_health>,
	[[<betterwithmods:material:46>,<betterwithmods:material:46>,<betterwithmods:material:46>],
	 [<betterwithmods:material:46>,<minecraft:golden_carrot>,<betterwithmods:material:46>],
	 [<betterwithmods:material:46>,<betterwithmods:material:46>,<betterwithmods:material:46>]]);

recipes.remove(<cyclicmagic:glove_climb>);
recipes.addShapedMirrored("cyclic_gloveClimb",<cyclicmagic:glove_climb>,
	[[<betterwithmods:material:45>,<betterwithmods:material:12>,<betterwithmods:material:6>],
	 [<betterwithmods:material:12>,<minecraft:ghast_tear>,<betterwithmods:material:6>],
	 [<betterwithmods:material:6>,<betterwithmods:material:6>,<twilightforest:fiery_ingot>]]);

recipes.remove(<cyclicmagic:purple_helmet>);
recipes.remove(<cyclicmagic:purple_chestplate>);
recipes.remove(<cyclicmagic:purple_leggings>);
recipes.remove(<cyclicmagic:purple_boots>);

recipes.addShaped("cyclic_copper_pickaxe", <cyclicmagic:sandstone_pickaxe>,
	[[<ore:ingotCopper>,<ore:ingotCopper>,<ore:ingotCopper>],
	[null, <ore:stickWood>, null],
	[null, <ore:stickWood>, null]]);
recipes.addShapedMirrored("cyclic_copper_axe", <cyclicmagic:sandstone_axe>,
	[[<ore:ingotCopper>,null,null],
	[<ore:ingotCopper>, <ore:stickWood>, null],
	[null, <ore:stickWood>, null]]);
recipes.addShaped("cyclic_copper_shovel", <cyclicmagic:sandstone_spade>,
	[[<ore:ingotCopper>],
	[ <ore:stickWood>],
	[ <ore:stickWood>]]);
recipes.addShapedMirrored("cyclic_copper_hoe", <cyclicmagic:sandstone_hoe>,
	[[<ore:ingotCopper>,<ore:ingotCopper>],
	[null, <ore:stickWood>],
	[null, <ore:stickWood>]]);
