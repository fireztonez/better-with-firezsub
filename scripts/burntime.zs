//Make this script top execution priority
#priority 700

import crafttweaker.item.IIngredient;
import crafttweaker.oredict.IOreDictEntry;
import crafttweaker.item.IItemStack;


var fuelBurnTime as int[IIngredient] = {
		<ore:plankOak> : 900,
		<ore:plankSpruce> : 700,
		<ore:plankBirch> : 1100,
		<ore:plankJungle> : 500,
		<ore:plankAcacia> : 700,
		<ore:plankDarkOak> : 900,
		<ore:plankYew> : 700,
		<ore:plankStained> : 600,
		<ore:logOak> : 3200,
		<ore:logSpruce> : 2400,
		<ore:logBirch> : 4200,
		<ore:logJungle> : 1600,
		<ore:logAcacia> : 3200,
		<ore:logDarkOak> : 1600,
	 	<ore:logYew> : 2400,
		<ore:stackedOak> : 1200,
		<ore:stackedSpruce> : 1000,
		<ore:stackedBirch> : 1400,
		<ore:stackedJungle> : 800,
		<ore:stackedAcacia> : 1200,
		<ore:stackedDarkOak> : 1000,
		<ore:stackedYew> : 1000,
		<twilightforest:giant_log> : 32000,
		<immersiveengineering:material:6> : 6400,
		<immersiveengineering:stone_decoration:3> : 64000,
		<ore:blockCoal> : 32000
} ;

for i in fuelBurnTime {
	furnace.setFuel(i, fuelBurnTime[i]);
}
