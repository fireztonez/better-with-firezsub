#priority 1149
#loader contenttweaker

import mods.contenttweaker.VanillaFactory;
import mods.contenttweaker.Block;
import mods.contenttweaker.Color;
import mods.contenttweaker.Item;
import mods.contenttweaker.Material;
import mods.contenttweaker.MaterialPartData;
import mods.contenttweaker.MaterialSystem;
import mods.contenttweaker.IItemColorSupplier;



var palladiumCatalistBlock = VanillaFactory.createBlock("palladium_catalyst", <blockmaterial:circuits>);
palladiumCatalistBlock.setLightOpacity(3);
palladiumCatalistBlock.setLightValue(0);
palladiumCatalistBlock.setBlockHardness(5.0);
palladiumCatalistBlock.setBlockResistance(5.0);
palladiumCatalistBlock.setToolClass("pickaxe");
palladiumCatalistBlock.setToolLevel(0);
palladiumCatalistBlock.setBlockSoundType(<soundtype:metal>);
palladiumCatalistBlock.setTranslucent(true);
palladiumCatalistBlock.setFullBlock(false);
palladiumCatalistBlock.setEntitySpawnable(false);
palladiumCatalistBlock.register();

//var antiIceBlock = VanillaFactory.createBlock("anti_ice", <blockmaterial:ice>);
//antiIceBlock.setLightOpacity(3);
//antiIceBlock.setLightValue(0);
//antiIceBlock.setBlockHardness(5.0);
//antiIceBlock.setBlockResistance(5.0);
//antiIceBlock.setToolClass("pickaxe");
//antiIceBlock.setToolLevel(0);
//antiIceBlock.setBlockSoundType(<soundtype:snow>);
//antiIceBlock.setSlipperiness(0.3);
//antiIceBlock.register();
