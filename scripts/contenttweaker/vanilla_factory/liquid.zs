#priority 1149
#loader contenttweaker

import mods.contenttweaker.Color;
import mods.contenttweaker.Fluid;
import mods.contenttweaker.VanillaFactory;

var liquidGreenSlime = VanillaFactory.createFluid("greenSlime", Color.fromHex("00AC42"));
liquidGreenSlime.vaporize = false;
liquidGreenSlime.viscosity = 3000;
liquidGreenSlime.density = 8000;
liquidGreenSlime.register();

var liquidBlueSlime = VanillaFactory.createFluid("blueSlime", Color.fromHex("009899"));
liquidBlueSlime.vaporize = false;
liquidBlueSlime.viscosity = 3000;
liquidBlueSlime.density = 8000;
liquidBlueSlime.register();

//var liquidPurpleSlime = VanillaFactory.createFluid("purpleSlime", Color.fromHex("8100CB"));
//liquidPurpleSlime.vaporize = false;
//liquidPurpleSlime.viscosity = 3000;
//liquidPurpleSlime.density = 8000;
//liquidPurpleSlime.register();

//var liquidNaphtha = VanillaFactory.createFluid("naphtha", Color.fromHex("B6807D"));
//liquidNaphtha.vaporize = false;
//liquidNaphtha.viscosity = 1500;
//liquidNaphtha.density = 8000;
//liquidNaphtha.register();

var liquidKerosene = VanillaFactory.createFluid("kerosene", Color.fromHex("cbebf2"));
liquidKerosene.vaporize = true;
liquidKerosene.viscosity = 750;
liquidKerosene.density = 8000;
liquidKerosene.register();

var liquidPropene = VanillaFactory.createFluid("propene", Color.fromHex("8e8e8e"));
liquidPropene.vaporize = true;
liquidPropene.viscosity = 1000;
liquidPropene.density = 8000;
liquidPropene.register();

//var liquidCryotheumData = VanillaFactory.createFluid("cryotheum", Color.fromHex("1FB9EB"));
//liquidCryotheumData.vaporize = false;
//liquidCryotheumData.viscosity = 1000;
//liquidCryotheumData.density = 8000;
//liquidCryotheumData.temperature = 10;
//liquidCryotheumData.register();
