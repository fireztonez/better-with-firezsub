#priority 1150
#loader contenttweaker
import mods.contenttweaker.VanillaFactory;
import mods.contenttweaker.Item;

var brokenEnderPearl = VanillaFactory.createItem("broken_enderpearl");
brokenEnderPearl.register();

var purifiedOpal = VanillaFactory.createItem("purified_opal");
purifiedOpal.register();

var pickaxeCopper = VanillaFactory.createItem("pickaxe_copper");
pickaxeCopper.maxDamage = 64;
pickaxeCopper.toolClass = "pickaxe";
pickaxeCopper.toolLevel = 2;
//pickaxeCopper.itemDestroySpeed = 3;
pickaxeCopper.register();

var axeCopper = VanillaFactory.createItem("axe_copper");
axeCopper.maxDamage = 92;
axeCopper.toolClass = "axe";
axeCopper.toolLevel = 2;
//axeCopper.itemDestroySpeed = 3;
axeCopper.register();

var shovelCopper = VanillaFactory.createItem("shovel_copper");
shovelCopper.maxDamage = 64;
shovelCopper.toolClass = "shovel";
shovelCopper.toolLevel = 2;
//shovelCopper.itemDestroySpeed = 3;
shovelCopper.register();

var pickaxeBronze = VanillaFactory.createItem("pickaxe_bronze");
pickaxeBronze.maxDamage = 150;
pickaxeBronze.toolClass = "pickaxe";
pickaxeBronze.toolLevel = 3;
//pickaxeBronze.itemDestroySpeed = 4;
pickaxeBronze.register();

var axeBronze = VanillaFactory.createItem("axe_bronze");
axeBronze.maxDamage = 175;
axeBronze.toolClass = "axe";
axeBronze.toolLevel = 3;
//axeBronze.itemDestroySpeed = 4;
axeBronze.register();

var shovelBronze = VanillaFactory.createItem("shovel_bronze");
shovelBronze.maxDamage = 150;
shovelBronze.toolClass = "shovel";
shovelBronze.toolLevel = 3;
//shovelBronze.itemDestroySpeed = 4;
shovelBronze.register();
