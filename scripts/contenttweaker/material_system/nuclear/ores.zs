#priority 1156
#loader contenttweaker

import mods.contenttweaker.Color;
import mods.contenttweaker.Material;
import mods.contenttweaker.MaterialPartData;
import mods.contenttweaker.MaterialSystem;

static materials as Material[string] = {
    "bertrandite": MaterialSystem.getMaterialBuilder().setName("Bertrandite").setColor(Color.fromHex("C6BC99")).build(), //Beryllium ores
    "rhodochrosite": MaterialSystem.getMaterialBuilder().setName("Rhodochrosite").setColor(Color.fromHex("B13D42")).build(),
    "fluorite": MaterialSystem.getMaterialBuilder().setName("Fluorite").setColor(Color.fromHex("3F656E")).build(),
    "zircon": MaterialSystem.getMaterialBuilder().setName("Zircon").setColor(Color.fromHex("371406")).build(),
    "zirconium": MaterialSystem.getMaterialBuilder().setName("Zirconium").setColor(Color.fromHex("7B6538")).build(),
    "villiaumite": MaterialSystem.getMaterialBuilder().setName("Villiaumite").setColor(Color.fromHex("9C0701")).build(),
    "carobbiite": MaterialSystem.getMaterialBuilder().setName("Carobbiite").setColor(Color.fromHex("8E7608")).build(),
};

materials.zirconium.registerPart("cluster").getData();
materials.villiaumite.registerPart("cluster").getData();


var oreBertranditeOreData = materials.bertrandite.registerPart("ore").getData(); //Bertrandite Ores (Berryllium can be drop as second output with Berryl to)
oreBertranditeOreData.addDataValue("drops", "contenttweaker:material_part:142");
var oreBertranditeSampleData = materials.bertrandite.registerPart("ore_sample").getData(); //Berryllium Ores (Berryllium can be drop as second output with Berryl to)
oreBertranditeSampleData.addDataValue("drops", "contenttweaker:material_part:142");

var oreRhodochrositeOreData = materials.rhodochrosite.registerPart("ore").getData(); //Rhodochrosite Gem ore
oreRhodochrositeOreData.addDataValue("drops", "nuclearcraft:gem");
var oreRhodochrositeSampleData = materials.rhodochrosite.registerPart("ore_sample").getData(); //Rhodochrosite Gem ore
oreRhodochrositeSampleData.addDataValue("drops", "nuclearcraft:gem");

var oreFluoriteOreData = materials.fluorite.registerPart("ore").getData(); // Gem ore
oreFluoriteOreData.addDataValue("drops", "nuclearcraft:gem:2");
var oreFluoriteSampleData = materials.fluorite.registerPart("ore_sample").getData(); // Gem ore
oreFluoriteSampleData.addDataValue("drops", "nuclearcraft:gem:2");

var oreZirconOreData = materials.zircon.registerPart("ore").getData(); //Zirconium ore
oreZirconOreData.addDataValue("drops", "contenttweaker:material_part:115");
var oreZirconSampleData = materials.zircon.registerPart("ore_sample").getData(); //Zirconium ore
oreZirconSampleData.addDataValue("drops", "contenttweaker:material_part:115");

var oreVilliaumiteOreData = materials.villiaumite.registerPart("ore").getData();
oreVilliaumiteOreData.addDataValue("drops", "contenttweaker:material_part:116");
var oreVilliaumiteSampleData = materials.villiaumite.registerPart("ore_sample").getData();
oreVilliaumiteSampleData.addDataValue("drops", "contenttweaker:material_part:116");

var oreCarobbiiteOreData = materials.carobbiite.registerPart("ore").getData();
oreCarobbiiteOreData.addDataValue("drops", "nuclearcraft:gem:4");
var oreCarobbiiteSampleData = materials.carobbiite.registerPart("ore_sample").getData();
oreCarobbiiteSampleData.addDataValue("drops", "nuclearcraft:gem:4");
