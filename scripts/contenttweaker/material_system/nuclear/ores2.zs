#priority 1156
#loader contenttweaker

import mods.contenttweaker.Color;
import mods.contenttweaker.Material;
import mods.contenttweaker.MaterialPartData;
import mods.contenttweaker.MaterialSystem;

static materials as Material[string] = {
    "be": MaterialSystem.getMaterialBuilder().setName("Be").setColor(Color.fromHex("7B6538")).build() //Beryllium ores
};

materials.be.registerPart("cluster").getData(); //Berryllium Ores (Berryllium can be drop as second output with Berryl to)
