#priority 1152
#loader contenttweaker

import mods.contenttweaker.Color;
import mods.contenttweaker.Material;
import mods.contenttweaker.MaterialPartData;
import mods.contenttweaker.MaterialSystem;


static materials as Material[string] = {
  "thorium": MaterialSystem.getMaterialBuilder().setName("Thorium").setColor(Color.fromHex("2D2D2D")).build(),
  "boron": MaterialSystem.getMaterialBuilder().setName("Boron").setColor(Color.fromHex("575757")).build(),
  "lithium": MaterialSystem.getMaterialBuilder().setName("Lithium").setColor(Color.fromHex("E7E7E7")).build(),
  "magnesium": MaterialSystem.getMaterialBuilder().setName("Magnesium").setColor(Color.fromHex("EBCFE9")).build(),
  "dust": MaterialSystem.getMaterialBuilder().setName("Dust").setColor(Color.fromHex("777777")).build()
};

var oreThoriumSampleData = materials.thorium.registerPart("ore_sample").getData();
oreThoriumSampleData.addDataValue("drops", "contenttweaker:material_part:52");
var oreThoriumClusterData = materials.thorium.registerPart("cluster").getData();

var oreBoronSampleData = materials.boron.registerPart("ore_sample").getData();
oreBoronSampleData.addDataValue("drops", "contenttweaker:material_part:54");
var oreBoronClusterData = materials.boron.registerPart("cluster").getData();

var oreLithiumSampleData = materials.lithium.registerPart("ore_sample").getData();
oreLithiumSampleData.addDataValue("drops", "contenttweaker:material_part:56");
var oreLithiumClusterData = materials.lithium.registerPart("cluster").getData();

var oreMagnesiumSampleData = materials.magnesium.registerPart("ore_sample").getData();
oreMagnesiumSampleData.addDataValue("drops", "contenttweaker:material_part:58");
var oreMagnesiumClusterData = materials.magnesium.registerPart("cluster").getData();


var oreDustSampleData = materials.dust.registerPart("ore_sample").getData();
oreDustSampleData.addDataValue("drops", "betterwithmods:gravel_pile");
