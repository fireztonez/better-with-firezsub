#priority 1155
#loader contenttweaker

import mods.contenttweaker.Color;
import mods.contenttweaker.Material;
import mods.contenttweaker.MaterialPartData;
import mods.contenttweaker.MaterialSystem;

static materials as Material[string] = {
	"tin": MaterialSystem.getMaterialBuilder().setName("Tin").setColor(10275286).build(),
	"bronze": MaterialSystem.getMaterialBuilder().setName("Bronze").setColor(Color.fromHex("7B6538")).build(),
	"copper": MaterialSystem.getMaterialBuilder().setName("Copper").setColor(Color.fromHex("DE7800")).build(),
	"redstone": MaterialSystem.getMaterialBuilder().setName("Redstone").setColor(Color.fromHex("FC0000")).build(),
	"redstoneAlloy": MaterialSystem.getMaterialBuilder().setName("Redstone Alloy").setColor(Color.fromHex("9E3232")).build(),
	"glowstone": MaterialSystem.getMaterialBuilder().setName("Glowstone").setColor(Color.fromHex("ffd454")).build(),
	"invar": MaterialSystem.getMaterialBuilder().setName("Invar").setColor(Color.fromHex("A5B5AD")).build(),
	"manyullyn": MaterialSystem.getMaterialBuilder().setName("Manyullyn").setColor(Color.fromHex("592187")).build(),
	"osmium": MaterialSystem.getMaterialBuilder().setName("Osmium").setColor(Color.fromHex("9DC2CF")).build(),
	"plastic": MaterialSystem.getMaterialBuilder().setName("Plastic").setColor(Color.fromHex("828282")).build(),
	"modularium": MaterialSystem.getMaterialBuilder().setName("Modularium").setColor(Color.fromHex("F64600")).build(),
	"emerald": MaterialSystem.getMaterialBuilder().setName("Emerald").setColor(Color.fromHex("248B2C")).build(),
	"palladium": MaterialSystem.getMaterialBuilder().setName("Palladium").setColor(Color.fromHex("BFC2B9")).build(),
	"steel": MaterialSystem.getMaterialBuilder().setName("Steel").setColor(Color.fromHex("4F5151")).build(),
	"enderium": MaterialSystem.getMaterialBuilder().setName("Enderium").setColor(Color.fromHex("318080")).build(),
	"cryotheum": MaterialSystem.getMaterialBuilder().setName("Cryotheum").setColor(Color.fromHex("1FB9EB")).build(),
	"platinum": MaterialSystem.getMaterialBuilder().setName("Platinum").setColor(Color.fromHex("88D5EB")).build(),
	"enderAlloy": MaterialSystem.getMaterialBuilder().setName("Ender Alloy").setColor(Color.fromHex("537E5A")).build(),
	"ardite": MaterialSystem.getMaterialBuilder().setName("Ardite").setColor(Color.fromHex("C94605")).build(),
	"cobalt": MaterialSystem.getMaterialBuilder().setName("Cobalt").setColor(Color.fromHex("216EBF")).build(),
	"manyullyn": MaterialSystem.getMaterialBuilder().setName("Manyullyn").setColor(Color.fromHex("5B2289")).build(),
	"gold": MaterialSystem.getMaterialBuilder().setName("Gold").setColor(Color.fromHex("E9BE4D")).build(),
	"soulforgedsteel": MaterialSystem.getMaterialBuilder().setName("Soulforged Steel").setColor(Color.fromHex("565351")).build(),
};

//var metal_list = [tin, bronze] as Material[];

materials.copper.registerPart("gear");
materials.tin.registerPart("gear");
materials.bronze.registerPart("gear");
materials.emerald.registerPart("ingot");

var materialPalladium as string[] = [
	"block",
	"ingot",
	"dust",
	"nugget",
	"plate",
	"rod",
	"molten"
];
materials.palladium.registerParts(materialPalladium);

materials.platinum.registerPart("rod");


var materialSteel as string[] = [
	"gear"
];
materials.steel.registerParts(materialSteel);

var materialBaseParts as string[] = [
	"ore",
	"block",
	"ingot",
	"nugget",
	"dust",
	"plate",
	"molten"
];
materials.osmium.registerParts(materialBaseParts);


var redstoneAlloyParts as string[] = [
	"block",
	"gear",
	"ingot",
	"nugget",
	"plate",
	"rod"
];
materials.redstoneAlloy.registerParts(redstoneAlloyParts);



var fluidRedstoneAlloyData = materials.redstoneAlloy.registerPart("molten").getData();
fluidRedstoneAlloyData.addDataValue("density", "8000");
fluidRedstoneAlloyData.addDataValue("viscosity", "1000");
fluidRedstoneAlloyData.addDataValue("temperature", "1000");
fluidRedstoneAlloyData.addDataValue("vaporize", "false");

//Liquid Redstone
var fluidRedstoneData = materials.redstone.registerPart("molten").getData();
fluidRedstoneData.addDataValue("density", "8000");
fluidRedstoneData.addDataValue("viscosity", "3000");
fluidRedstoneData.addDataValue("temperature", "1000");
fluidRedstoneData.addDataValue("vaporize", "false");

//Liquid Glowstone
var fluidGlowstoneData = materials.glowstone.registerPart("molten").getData();
fluidGlowstoneData.addDataValue("density", "500");
fluidGlowstoneData.addDataValue("viscosity", "3000");
fluidGlowstoneData.addDataValue("temperature", "750");
fluidGlowstoneData.addDataValue("vaporize", "false");

//Liquid Plastic
var fluidPlasticData = materials.plastic.registerPart("molten").getData();
fluidPlasticData.addDataValue("density", "8000");
fluidPlasticData.addDataValue("viscosity", "2000");
fluidPlasticData.addDataValue("temperature", "600");
fluidPlasticData.addDataValue("vaporize", "false");

var invarParts as string[] = [
	//"block",
	//"gear",
	//"ingot",
	//"plate",
	"rod"
	//"nugget",
	//"molten"
];
materials.invar.registerParts(invarParts);
materials.enderium.registerParts(invarParts);

var modulariumParts as string[] = [
	"block",
	"gear",
	"plate",
	"rod",
	"nugget",
	"molten"
];
materials.modularium.registerParts(modulariumParts);

var tinkerMetalsParts as string[] = [
	"dust",
	"gear",
	"plate",
	"rod"
];
materials.ardite.registerParts(tinkerMetalsParts);
materials.cobalt.registerParts(tinkerMetalsParts);
materials.manyullyn.registerParts(tinkerMetalsParts);

materials.gold.registerPart("rod");

materials.soulforgedsteel.registerPart("molten");
