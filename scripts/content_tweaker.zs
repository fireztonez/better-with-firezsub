//Invar Block
recipes.addShaped("own_invarBlock", metalItems.invar.block,
	[[<ore:ingotInvar>,<ore:ingotInvar>,<ore:ingotInvar>],
	 [<ore:ingotInvar>,<ore:ingotInvar>,<ore:ingotInvar>],
	 [<ore:ingotInvar>,<ore:ingotInvar>,<ore:ingotInvar>]]);

recipes.addShaped("own_invarIngot", metalItems.invar.ingot,
	[[<ore:nuggetInvar>,<ore:nuggetInvar>,<ore:nuggetInvar>],
	 [<ore:nuggetInvar>,<ore:nuggetInvar>,<ore:nuggetInvar>],
	 [<ore:nuggetInvar>,<ore:nuggetInvar>,<ore:nuggetInvar>]]);

recipes.addShapeless("own_invarIngotToNugget", metalItems.invar.nugget*9,
	[<ore:ingotInvar>]);

recipes.addShapeless("own_invarBlockToIngot", metalItems.invar.ingot*9,
	[<ore:blockInvar>]);

//Redstone Alloy
recipes.addShaped("own_RedstoneAlloyBlock", metalItems.redstoneAlloy.block,
	[[<ore:ingotRedstoneAlloy>,<ore:ingotRedstoneAlloy>,<ore:ingotRedstoneAlloy>],
	 [<ore:ingotRedstoneAlloy>,<ore:ingotRedstoneAlloy>,<ore:ingotRedstoneAlloy>],
	 [<ore:ingotRedstoneAlloy>,<ore:ingotRedstoneAlloy>,<ore:ingotRedstoneAlloy>]]);

recipes.addShaped("own_RedstoneAlloyIngot", metalItems.redstoneAlloy.ingot,
	[[<ore:nuggetRedstoneAlloy>,<ore:nuggetRedstoneAlloy>,<ore:nuggetRedstoneAlloy>],
	 [<ore:nuggetRedstoneAlloy>,<ore:nuggetRedstoneAlloy>,<ore:nuggetRedstoneAlloy>],
	 [<ore:nuggetRedstoneAlloy>,<ore:nuggetRedstoneAlloy>,<ore:nuggetRedstoneAlloy>]]);

recipes.addShapeless("own_RedstoneAlloyIngotToNugget", metalItems.redstoneAlloy.nugget*9,
	[<ore:ingotRedstoneAlloy>]);

recipes.addShapeless("own_RedstoneAlloyBlockToIngot", metalItems.redstoneAlloy.ingot*9,
	[<ore:blockRedstoneAlloy>]);

recipes.addShaped("contenttweaker_palladium_catalyst", <contenttweaker:palladium_catalyst>,
	[[metalItems.palladium.plate,null,metalItems.palladium.plate],
	 [null,metalItems.palladium.plate,null],
	 [metalItems.palladium.plate,null,metalItems.palladium.plate]]);

recipes.addShapeless("contenttweaker_emeraldIngot", <contenttweaker:material_part:27>, [<minecraft:gold_ingot>,<betterwithmods:creeper_oyster>,<minecraft:emerald>]);
