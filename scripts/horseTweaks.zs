val frostwalkbook = <minecraft:enchanted_book>.withTag({StoredEnchantments: [{lvl: 1 as short, id: 9 as short}]});
val swimSaddel = <minecraft:saddle>.withTag({HorseTweaksUpgrades: {SWIMMING: 1 as byte}});
val jumpSaddel = <minecraft:saddle>.withTag({HorseTweaksUpgrades: {EASY_JUMP: 1 as byte}});
val thornsSaddel = <minecraft:saddle>.withTag({HorseTweaksUpgrades: {THORNS: 1 as byte}});
val frostSaddel = <minecraft:saddle>.withTag({HorseTweaksUpgrades: {FROST_WALKER: 1 as byte}});
val leafSaddel = <minecraft:saddle>.withTag({HorseTweaksUpgrades: {LEAF_WALKER: 1 as byte}});
val fallSaddel = <minecraft:saddle>.withTag({HorseTweaksUpgrades: {FEATHER_FALL: 1 as byte}});
val fireSaddel = <minecraft:saddle>.withTag({HorseTweaksUpgrades: {FIRE_RESISTANCE: 1 as byte}});
val ALLSaddel = <minecraft:saddle>.withTag({HorseTweaksUpgrades: {FIRE_RESISTANCE: 1 as byte, LEAF_WALKER: 1 as byte, FEATHER_FALL: 1 as byte, EASY_JUMP: 1 as byte, THORNS: 1 as byte, SWIMMING: 1 as byte, FROST_WALKER: 1 as byte}});

recipes.remove(swimSaddel);
recipes.remove(jumpSaddel);
recipes.remove(thornsSaddel);
recipes.remove(frostSaddel);
recipes.remove(leafSaddel);
recipes.remove(fallSaddel);
recipes.remove(fireSaddel);
recipes.remove(ALLSaddel);


recipes.addShaped(swimSaddel, [
	[<minecraft:saddle>, <ore:slabWood>, <ore:slabWood>],
	[<ore:leather>, <ore:leather>, <ore:leather>]
	]);

recipes.addShaped(jumpSaddel, [
	[<minecraft:saddle>, <ore:slimeball>, <ore:slimeball>],
	[<ore:leather>, <ore:leather>, <ore:leather>]
	]);
	
recipes.addShaped(thornsSaddel, [
	[<minecraft:saddle>, <minecraft:cactus>, <minecraft:cactus>],
	[<ore:leather>, <ore:leather>, <ore:leather>]
	]);
	
recipes.addShaped(frostSaddel, [
	[<minecraft:saddle>, frostwalkbook, null],
	[<ore:leather>, <ore:leather>, <ore:leather>]
	]);
	
recipes.addShaped(leafSaddel, [
	[<minecraft:saddle>, <ore:treeLeaves>, <ore:treeLeaves>],
	[<ore:leather>, <ore:leather>, <ore:leather>]
	]);
	
recipes.addShaped(fallSaddel, [
	[<minecraft:saddle>, <ore:feather>, <ore:feather>],
	[<ore:leather>, <ore:leather>, <ore:leather>]
	]);
	
recipes.addShaped(fireSaddel, [
	[<minecraft:saddle>, <minecraft:magma_cream>, <minecraft:magma_cream>],
	[<ore:leather>, <ore:leather>, <ore:leather>]
	]);
	
//recipes.addShapeless(ALLSaddel, [swimSaddel, jumpSaddel, frostSaddel, thornsSaddel, leafSaddel, fallSaddel, fireSaddel]);