import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import mods.betterwithmods.Crucible;
import mods.betterwithmods.Kiln;
import mods.betterwithmods.Cauldron;
import mods.betterwithmods.Anvil;

var anyChest = <ore:chest>;
var stick = <ore:stickWood>;
var siding = <betterwithmods:siding_wood>;
var moulding = <betterwithmods:moulding_wood>;
var sfsIngot = <ore:ingotSoulforgedSteel>;

recipes.remove(<storagedrawers:controller>);
mods.betterwithmods.Anvil.addShaped(<storagedrawers:controller>, [
    [<minecraft:stone:*>, <storagedrawers:trim:*>, <storagedrawers:trim:*>, sfsIngot],
    [<minecraft:stone:*>, <minecraft:ender_eye>, <betterwithmods:material:34>, sfsIngot],
    [<minecraft:stone:*>, <minecraft:ender_eye>, <betterwithmods:material:34>, sfsIngot],
    [<minecraft:stone:*>, <storagedrawers:trim:*>, <storagedrawers:trim:*>, sfsIngot]
]);

var chainMail = <betterwithmods:material:47>;
recipes.remove(<wolfarmor:chainmail_wolf_armor>);
recipes.addShaped("wolfarmor_chainmail", <wolfarmor:chainmail_wolf_armor>, [
    [chainMail, null, null],
    [chainMail, chainMail, chainMail],
    [chainMail, null, chainMail]
]);


recipes.remove(<betterwithmods:lens>);
recipes.addShaped("bwa_lens",<betterwithmods:lens>,
	[[<immersiveengineering:metal:40>, <primal:diamond_plate>, <immersiveengineering:metal:40>],
	 [<immersiveengineering:metal:40>, null, <immersiveengineering:metal:40>],
	 [<immersiveengineering:metal:40>, <ore:blockGlass>, <immersiveengineering:metal:40>]]);


recipes.addShaped("tripwire_hook", <minecraft:tripwire_hook>,
    [[null,<primal:iron_ring>,null],
     [null,<ore:stickWood>,null],
     [<minecraft:redstone>,sidingPlankWood,<minecraft:redstone>]]);
