#priority 800
import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import crafttweaker.oredict.IOreDictEntry;
import mods.betterwithmods.MiniBlocks;


recipes.remove(<immersiveengineering:stone_decoration:10>);
recipes.addShapedMirrored("kilnBricks",<immersiveengineering:stone_decoration:10>,
	[[<primal:adobe_brick_dry>,<minecraft:brick>],
	 [<minecraft:brick>,<primal:adobe_brick_dry>]]);

recipes.remove(<immersiveengineering:metal_decoration0:3>);
recipes.addShaped("redstone_engineering_block",<immersiveengineering:metal_decoration0:3>,
	[[<ore:plateIron>,<ore:plateRedstoneAlloy>,<ore:plateIron>],
	 [<ore:plateRedstoneAlloy>,<immersiveengineering:material:27>,<ore:plateRedstoneAlloy>],
	 [<ore:plateIron>,<ore:plateRedstoneAlloy>,<ore:plateIron>]]);

recipes.remove(<immersiveengineering:metal_device0>);
recipes.addShaped("immEng_lv-capacitor",<immersiveengineering:metal_device0>,
	[[<ore:ingotIron>,<ore:ingotIron>,<ore:ingotIron>],
	 [<ore:plateCopper>,<ore:ingotLead>,<ore:plateCopper>],
	 [sidingTreatedWood,<ore:plateRedstoneAlloy>,sidingTreatedWood]]);

recipes.remove(<immersiveengineering:metal_device0:1>);
recipes.addShaped("immEng_mv-capacitor",<immersiveengineering:metal_device0:1>,
	[[<ore:ingotInvar>,<ore:ingotInvar>,<ore:ingotInvar>],
	 [<ore:plateElectrum>,<ore:ingotLead>,<ore:plateElectrum>],
	 [sidingTreatedWood,<ore:blockRedstoneAlloy>,sidingTreatedWood]]);

recipes.remove(<immersiveengineering:metal_device0:2>);
recipes.addShaped("immEng_hv-capacitor",<immersiveengineering:metal_device0:2>,
	[[<ore:ingotSteel>,<ore:ingotSteel>,<ore:ingotSteel>],
	 [<ore:plateAluminum>,<ore:blockLead>,<ore:plateAluminum>],
	 [sidingTreatedWood,<ore:blockRedstoneAlloy>,sidingTreatedWood]]);

recipes.addShapedMirrored("immersive_engineersHammer", <immersiveengineering:tool>,
	[[null,<ore:ingotIron>,<ore:string>],
	 [null,<ore:stickIronwood>,<ore:ingotIron>],
	 [<ore:stickIronwood>,null,null]]);

recipes.addShaped("immersive_cokeBricks", <quark:charcoal_block>,
	[[clayball,brick, clayball],
	[brick,<ore:blockCharcoal>,brick],
	[clayball,brick, clayball]]);
