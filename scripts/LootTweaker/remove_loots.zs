import loottweaker.vanilla.loot.LootTables;
import loottweaker.vanilla.loot.LootTable;
import loottweaker.vanilla.loot.LootPool;
import loottweaker.vanilla.loot.Conditions;
import loottweaker.vanilla.loot.Functions;

//Get the loot table named "minecraft:entities/pig" and store it for later use
val endCityChest = LootTables.getTable("minecraft:chests/end_city_treasure");
//Get the pool named "main" from the loot table and store it for later use
val endCityChest_main = endCityChest.getPool("main");
//Remove the entry named "minecraft:porkchop" from the loot pool


var removeEntry_endCityChest as string[] = [
    "actuallyadditions:crystalBlocks",
    "actuallyadditions:crystalItems",
    "actuallyadditions:drillCore",
    "minecraft:emerald",
    "minecraft:diamond"
];

endCityChest_main.addItemEntry(<minecraft:golden_apple:1>, 5);
endCityChest_main.addItemEntry(<minecraft:golden_apple>, 15);
endCityChest_main.addItemEntry(<minecraft:golden_apple>, 15);
endCityChest_main.addItemEntry(<betterwithmods:beef_dinner>, 30);
endCityChest_main.addItemEntry(<betterwithmods:material:5>*3, 15);
endCityChest_main.addItemEntry(<betterwithmods:material:5>*3, 15);

val simpleDungeonChest = LootTables.getTable("minecraft:chests/simple_dungeon");
val simpleDungeonChest_main = simpleDungeonChest.getPool("main");

var removeEntry_simpleDungeonChest as string[] = [
    "abyssalcraft:abyssalnite_ingot",
    "abyssalcraft:copper_ingot",
    "abyssalcraft:tin_ingot",
    "abyssalcraft:crystallized_zinc",
    "abyssalcraft:mre",
    "abyssalcraft:coralium_gem",
    "abyssalcraft:shadow_gem",
    "cyberware:neuropozyne",
];

for i in removeEntry_simpleDungeonChest {
    simpleDungeonChest_main.removeEntry(i);
}

val simpleDungeonPneumaticraft = LootTables.getTable("pneumaticcraft:inject/simple_dungeon_loot");
val simpleDungeonPneumaticraft_main = simpleDungeonPneumaticraft.getPool("main");

var removeEntry_simpleDungeonPneumaticraft as string[] = [
    "pneumaticcraft:spawner_agitator"
];

for i in removeEntry_simpleDungeonPneumaticraft {
    simpleDungeonPneumaticraft_main.removeEntry(i);
}

//var removeEntry as String[string][] = {
//    endCityChest: [
//        "actuallyadditions:crystalBlocks",
//        "actuallyadditions:crystalItems",
//        "actuallyadditions:drillCore",
//        "minecraft:emerald",
//        "minecraft:diamond"
//    ],
//    simpleDungeonChest: [
//        "actuallyadditions:crystalBlocks",
//        "actuallyadditions:crystalItems",
//        "actuallyadditions:drillCore",
//        "minecraft:emerald",
//        "minecraft:diamond"
//    ]
//};
//
//for table in removeEntry {
//    for i in table {
//        table.removeEntry(i);
//    }
//}
