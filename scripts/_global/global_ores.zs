//Make this script top execution priority
#priority 1852

import crafttweaker.item.IItemStack;
import crafttweaker.oredict.IOreDictEntry;


global sampleDust as IItemStack = <contenttweaker:sub_block_holder_0:4>;

global sampleHematite as IItemStack = <geolosys:ore_sample>;
global sampleLimonite as IItemStack = <geolosys:ore_sample:1>;
global sampleMalachite as IItemStack = <geolosys:ore_sample:2>;
global sampleAzurite as IItemStack = <geolosys:ore_sample:3>;
global sampleCassiterite as IItemStack = <geolosys:ore_sample:4>;
global sampleTeallite as IItemStack = <geolosys:ore_sample:5>;
global sampleGalena as IItemStack = <geolosys:ore_sample:6>;
global sampleBauxite as IItemStack = <geolosys:ore_sample:7>;
global samplePlatinum as IItemStack = <geolosys:ore_sample:8>;
global sampleAutunite as IItemStack = <geolosys:ore_sample:9>;
global sampleSphalerite as IItemStack = <geolosys:ore_sample:10>;
global sampleCoal as IItemStack = <geolosys:ore_sample_vanilla>;
global sampleCinnabar as IItemStack = <geolosys:ore_sample_vanilla:1>;
global sampleGold as IItemStack = <geolosys:ore_sample_vanilla:2>;
global sampleLapis as IItemStack = <geolosys:ore_sample_vanilla:3>;
global sampleAssortedQuartz as IItemStack = <geolosys:ore_sample_vanilla:4>;
global sampleKimberlite as IItemStack = <geolosys:ore_sample_vanilla:5>;
global sampleBeryl as IItemStack = <geolosys:ore_sample_vanilla:6>;



global oreBlockHematite as IItemStack = <geolosys:ore>;
global oreBlockLimonite as IItemStack = <geolosys:ore:1>;
global oreBlockMalachite as IItemStack = <geolosys:ore:2>;
global oreBlockAzurite as IItemStack = <geolosys:ore:3>;
global oreBlockCassiterite as IItemStack = <geolosys:ore:4>;
global oreBlockTeallite as IItemStack = <geolosys:ore:5>;
global oreBlockGalena as IItemStack = <geolosys:ore:6>;
global oreBlockBauxite as IItemStack = <geolosys:ore:7>;
global oreBlockPlatinum as IItemStack = <geolosys:ore:8>;
global oreBlockAutunite as IItemStack = <geolosys:ore:9>;
global oreBlockSphalerite as IItemStack = <geolosys:ore:10>;
global oreBlockCoal as IItemStack = <geolosys:ore_vanilla>;
global oreBlockCinnabar as IItemStack = <geolosys:ore_vanilla:1>;
global oreBlockGold as IItemStack = <geolosys:ore_vanilla:2>;
global oreBlockLapis as IItemStack = <geolosys:ore_vanilla:3>;
global oreBlockAssortedQuartz as IItemStack = <geolosys:ore_vanilla:4>;
global oreBlockKimberlite as IItemStack = <geolosys:ore_vanilla:5>;
global oreBlockBeryl as IItemStack = <geolosys:ore_vanilla:6>;

global oreGold as IItemStack =  <minecraft:gold_ore>;
global oreIron as IItemStack =  <minecraft:iron_ore>;
global oreCoal as IItemStack =  <minecraft:coal_ore>;
global oreLapis as IItemStack =  <minecraft:lapis_ore>;
global oreDiamond as IItemStack =  <minecraft:diamond_ore>;
global oreRedstone as IItemStack =  <minecraft:redstone_ore>;
global oreEmerald as IItemStack =  <minecraft:emerald_ore>;
global oreQuartz as IItemStack =  <minecraft:quartz_ore>;
global oreCertusQuartz as IItemStack =  <appliedenergistics2:quartz_ore>;
global oreAstralRockCrystal as IItemStack =  <astralsorcery:blockcustomore>;
global oreAstralStarmetal as IItemStack =  <astralsorcery:blockcustomore:1>;

//NuclearCraft
global sampleFluorite as IItemStack = <contenttweaker:sub_block_holder_0:15>;
global sampleZircon as IItemStack = <contenttweaker:sub_block_holder_0:12>;
global sampleBertrandite as IItemStack = <contenttweaker:sub_block_holder_1>;
global sampleVilliaumite as IItemStack = <contenttweaker:sub_block_holder_1:4>;
global sampleCarobbiite as IItemStack = <contenttweaker:sub_block_holder_1:6>;
global sampleRhodochrosite as IItemStack = <contenttweaker:sub_block_holder_1:7>;
global sampleThorium as IItemStack = <contenttweaker:sub_block_holder_0>;
global sampleMagnesium as IItemStack = <contenttweaker:sub_block_holder_0:1>;
global sampleLithium as IItemStack = <contenttweaker:sub_block_holder_0:2>;
global sampleBoron as IItemStack = <contenttweaker:sub_block_holder_0:3>;

global oreFluorite as IItemStack = <contenttweaker:sub_block_holder_0:13>;
global oreZircon as IItemStack = <contenttweaker:sub_block_holder_1:1>;
global oreBertrandite as IItemStack = <contenttweaker:sub_block_holder_1:3>;
global oreVilliaumite as IItemStack = <contenttweaker:sub_block_holder_0:14>;
global oreCarobbiite as IItemStack = <contenttweaker:sub_block_holder_1:5>;
global oreRhodochrosite as IItemStack = <contenttweaker:sub_block_holder_1:2>;
global oreThorium as IItemStack = <nuclearcraft:ore:3>;
global oreMagnesium as IItemStack = <nuclearcraft:ore:7>;
global oreLithium as IItemStack = <nuclearcraft:ore:6>;
global oreBoron as IItemStack = <nuclearcraft:ore:5>;


global clusterBeryllium as IItemStack = <contenttweaker:material_part:142>;

global oresClustersItems as IItemStack[string][string] = {
	iron: {
        ores: <minecraft:iron_ore>,
        cluster: <geolosys:cluster>
    },
	gold: {
        ores: <minecraft:gold_ore>,
        cluster: <geolosys:cluster:1>
    },
	copper: {
        ores: <immersiveengineering:ore>,
        cluster: <geolosys:cluster:2>
    },
	tin: {
        ores: <thermalfoundation:ore:1>,
        cluster: <geolosys:cluster:3>
    },
	silver: {
        ores: <immersiveengineering:ore:3>,
        cluster: <geolosys:cluster:4>
    },
	lead: {
        ores: <immersiveengineering:ore:2>,
        cluster: <geolosys:cluster:5>
    },
	aluminum: {
        ores: <immersiveengineering:ore:1>,
        cluster: <geolosys:cluster:6>
    },
	nickel: {
        ores: <immersiveengineering:ore:4>,
        cluster: <geolosys:cluster:7>
    },
	platinum: {
        ores: <contenttweaker:sub_block_holder_1:9>,
        cluster: <geolosys:cluster:8>
    },
	uranium: {
        ores: <immersiveengineering:ore:5>,
        cluster: <geolosys:cluster:9>
    },
	zinc: {
        ores: <geolosys:ore:10>,
        cluster: <geolosys:cluster:10>
    },
	osmium: {
        ores: <contenttweaker:sub_block_holder_1:8>,
        cluster: <geolosys:cluster:12>
    },
    beryllium: {
        ores: <contenttweaker:material_part:142>,
        cluster: <contenttweaker:sub_block_holder_1:3>
    },
    thorium: {
        ores: <contenttweaker:material_part:52>,
        cluster: <nuclearcraft:ore:3>
    },
    boron: {
        ores: <contenttweaker:material_part:54>,
        cluster: <nuclearcraft:ore:5>
    },
    lithium: {
        ores: <contenttweaker:material_part:56>,
        cluster: <nuclearcraft:ore:6>
    },
    magnesium: {
        ores: <contenttweaker:material_part:58>,
        cluster: <nuclearcraft:ore:7>
    },
    zirconium: {
        ores: <contenttweaker:material_part:115>,
        cluster: <contenttweaker:sub_block_holder_1:1>
    },
    viliaumite: {
        ores: <contenttweaker:material_part:116>,
        cluster: <contenttweaker:sub_block_holder_0:14>
    }
};
