//Make this script top execution priority
#priority 999

import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import crafttweaker.oredict.IOreDictEntry;
import mods.betterwithmods.MiniBlocks;

#mods.betterwithmods.MiniBlocks.getMiniBlock(String type, IIngredient parentBlock)
#TYPE: “siding”, “moulding” or “corner”

global sidingStone as IIngredient = mods.betterwithmods.MiniBlocks.getMiniBlock("siding", <minecraft:stone>);
global sidingPlankWood as IIngredient = mods.betterwithmods.MiniBlocks.getMiniBlock("siding", <ore:plankWood>);
global mouldingPlankWood as IIngredient = mods.betterwithmods.MiniBlocks.getMiniBlock("moulding", <ore:plankWood>);
#Specific vanilla planks
global sidingOakWood as IIngredient = mods.betterwithmods.MiniBlocks.getMiniBlock("siding", <ore:plankOak>);
global sidingSpruceWood as IIngredient = mods.betterwithmods.MiniBlocks.getMiniBlock("siding", <ore:plankSpruce>);
global sidingBirchWood as IIngredient = mods.betterwithmods.MiniBlocks.getMiniBlock("siding", <ore:plankBirch>);
global sidingJungleWood as IIngredient = mods.betterwithmods.MiniBlocks.getMiniBlock("siding", <ore:plankJungle>);
global sidingAcaciaWood as IIngredient = mods.betterwithmods.MiniBlocks.getMiniBlock("siding", <ore:plankAcacia>);
global sidingDarkOakWood as IIngredient = mods.betterwithmods.MiniBlocks.getMiniBlock("siding", <ore:plankDarkOak>);

global moulding_soulforgedsteel as IIngredient = <betterwithmods:moulding_iron>.withTag({texture: {Properties: {height: "0"}, Name: "betterwithmods:steel_block"}});

global sidingTreatedWood as IIngredient = mods.betterwithmods.MiniBlocks.getMiniBlock("siding", <ore:plankTreatedWood>);
