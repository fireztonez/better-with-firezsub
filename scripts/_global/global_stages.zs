#priority 2001

//Unique global to store stages

global STAGES as string[string] = {
	//Core Stages
	beginner : "beginner",
	advanced : "advanced",
	nuclear : "nuclear",

	//Unique Stages
	baykok : "baykok",
	mapping : "mapping",

	//Unique stage intended to disable a tool
	disabled : "disabled"
};
