//Make this script top execution priority
#priority 1851

import crafttweaker.item.IItemStack;
import crafttweaker.oredict.IOreDictEntry;

global bone as IItemStack = <minecraft:bone>;
global brick as IItemStack = <minecraft:brick>;
global cane as IItemStack = <minecraft:reeds>;
global charcoal as IItemStack = <minecraft:coal:1>;
global clayball as IItemStack = <minecraft:clay_ball>;
global coal as IItemStack = <minecraft:coal>;
global cobblestone as IItemStack = <minecraft:cobblestone>;
global cordage as IOreDictEntry = <ore:cordageGeneral>;
global diamond as IItemStack = <minecraft:diamond>;
global emerald as IItemStack = <minecraft:emerald>;
global flint as IItemStack = <minecraft:flint>;
global lapis as IItemStack = <minecraft:dye:4>;
global leather as IItemStack = <minecraft:leather>;
global log as IOreDictEntry = <ore:logWood>;
global plank as IOreDictEntry = <ore:plankWood>;
global plastic as IItemStack = <industrialforegoing:plastic>;
global redstone as IItemStack = <minecraft:redstone>;
global latchRedstone as IItemStack = <betterwithmods:material:34>;
global shears as IOreDictEntry = <ore:shears>;
global slag as IItemStack = <immersiveengineering:material:7>;
global stick as IItemStack = <minecraft:stick>;
global stone as IItemStack = <minecraft:stone>;
global str as IItemStack = <minecraft:string>;
global wool as IOreDictEntry = <ore:wool>;
global water as IOreDictEntry = <ore:water>;
global hempString as IItemStack = <betterwithmods:material:3>;
global hempCloth as IItemStack = <betterwithmods:material:4>;
global rope as IItemStack = <betterwithmods:rope>;
global leatherCordage as IItemStack = <primal:leather_cordage>;
global mysteriousGland as IItemStack = <betterwithmods:material:53>;
global sulfur as IOreDictEntry = <ore:dustSulfur>;
global poisonSac as IItemStack = <betterwithmods:material:54>;
global witchWart as IItemStack = <betterwithmods:material:52>;
global spiderEye as IItemStack = <minecraft:spider_eye>;
global creeperOyster as IItemStack = <betterwithmods:creeper_oyster>;
global sugar as IItemStack = <minecraft:sugar>;

global diamondIngot as IItemStack = <betterwithmods:material:45>;
