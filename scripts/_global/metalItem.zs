#priority 750

import crafttweaker.item.IIngredient;
import crafttweaker.item.IItemStack;

//Metal unification
/*
	METAL_TYPE: {
		block: ITEM_STACK,
		dust: ITEM_STACK,
		gear: ITEM_STACK,
		ingot: ITEM_STACK,
		//liquid: <liquid:lava>,
		nugget: ITEM_STACK,
		plate: ITEM_STACK,
		rod: ITEM_STACK
	}
*/

global metalItems as IItemStack[string][string] = {
	iron: {
        block: <minecraft:iron_block>,
		dust: <immersiveengineering:metal:18>,
		gear: <thermalfoundation:material:24>,
		ingot: <minecraft:iron_ingot>,
		nugget: <minecraft:iron_nugget>,
		plate: <immersiveengineering:metal:39>,
		rod: <immersiveengineering:material:1>
        //liquid: <liquid:iron>
	},
	copper: {
		block: <immersiveengineering:storage>,
		gear: <immersiveengineering:metal:9>,
		dust: <contenttweaker:material_part:60>,
		ingot: <immersiveengineering:metal>,
		nugget: <immersiveengineering:metal:20>,
		plate: <immersiveengineering:metal:30>
		//liquid: <liquid:copper>
	},
	tin: {
		block: <primal:metalblock:9>,
		gear: <primal:tin_dust>,
		dust: <contenttweaker:material_part:61>,
		ingot: <primal:tin_ingot>,
		nugget: <primal:tin_nugget>,
		plate: <primal:tin_plate>
		//liquid: <liquid:tin>
	},
	bronze: {
		block: <primal:metalblock:11>,
		gear: <primal:bronze_dust>,
		dust: <contenttweaker:material_part:62>,
		ingot: <primal:bronze_ingot>,
		nugget: <primal:bronze_nugget>,
		plate: <primal:bronze_plate>
		//liquid: <liquid:bronze>
	},
	zinc: {
		block: <primal:metalblock:8>,
		dust: <primal:zinc_dust>,
		ingot: <geolosys:ingot:7>,
		nugget: <primal:zinc_nugget>,
		plate: <primal:zinc_plate>
		//liquid: <liquid:zinc>
	},
	brass: {
		block: <primal:metalblock:12>,
		dust: <primal:brass_dust>,
		ingot: <primal:brass_ingot>,
		nugget: <primal:brass_nugget>,
		plate: <primal:brass_plate>
		//liquid: <liquid:brass>
	},
	aluminumBrass: {
		block: <tconstruct:metal:5>,
		ingot: <tconstruct:ingots:5>,
		nugget: <tconstruct:nuggets:5>
		//liquid: <liquid:alubrass>
	},
    osmium: {
        block: <contenttweaker:sub_block_holder_0:11>,
        dust: <contenttweaker:material_part:82>,
        ingot: <contenttweaker:material_part:80>,
        nugget: <contenttweaker:material_part:81>,
        plate: <contenttweaker:material_part:83>
        //liquid: <liquid:osmium>
    },
    invar: {
        block: <thermalfoundation:storage_alloy:2>,
        gear: <thermalfoundation:material:290>,
        ingot: <thermalfoundation:material:162>,
        nugget: <thermalfoundation:material:226>,
        plate: <thermalfoundation:material:354>,
        rod: <thermalfoundation:material:99>
        //liquid: <liquid:invar>
    },
    redstoneAlloy: {
        block: <contenttweaker:sub_block_holder_0:7>,
        gear: <contenttweaker:material_part:86>,
        ingot: <contenttweaker:material_part:87>,
        nugget: <contenttweaker:material_part:88>,
        plate: <contenttweaker:material_part:89>,
        rod: <contenttweaker:material_part:90>
        //liquid: <liquid:redstone_alloy>
    },
    palladium: {
        block: <contenttweaker:sub_block_holder_0:10>,
        dust: <contenttweaker:material_part:66>,
        ingot: <contenttweaker:material_part:65>,
        nugget: <contenttweaker:material_part:67>,
        plate: <contenttweaker:material_part:68>,
        rod: <contenttweaker:material_part:69>
        //liquid: <liquid:palladium>
    },
    enderium: {
        block: <contenttweaker:sub_block_holder_0:8>,
        gear: <contenttweaker:material_part:103>,
        ingot: <contenttweaker:material_part:104>,
        nugget: <contenttweaker:material_part:107>,
        plate: <contenttweaker:material_part:105>,
        rod: <contenttweaker:material_part:106>
        //liquid: <liquid:enderium>
    },
    platinum: {
        block: <thermalfoundation:storage:6>,
        dust: <contenttweaker:material_part:76>,
        gear: <contenttweaker:material_part:72>,
        ingot: <geolosys:ingot:6>,
        nugget: <contenttweaker:material_part:73>,
        plate: <contenttweaker:material_part:74>,
        rod: <contenttweaker:material_part:75>
        //liquid: <liquid:platinum>
    },
    steel: {
        block: <immersiveengineering:storage:8>,
        dust: <immersiveengineering:metal:17>,
        gear: <contenttweaker:material_part:78>,
        ingot: <immersiveengineering:metal:8>,
        nugget: <immersiveengineering:metal:28>,
        plate: <immersiveengineering:metal:38>,
        rod: <immersiveengineering:material:2>
        //liquid: <liquid:steel>
    },
	modularium: {
		block: <contenttweaker:sub_block_holder_0:5>,
		gear: <contenttweaker:material_part:110>,
		ingot: <modularmachinery:itemmodularium>,
		nugget: <contenttweaker:material_part:113>,
		plate: <contenttweaker:material_part:111>,
		rod: <contenttweaker:material_part:112>
		//liquid: <liquid:modularium>
	},
	uranium: {
		block: <immersiveengineering:storage:5>,
		dust: <immersiveengineering:metal:14>,
		ingot: <immersiveengineering:metal:5>,
		nugget: <immersiveengineering:metal:25>,
		plate: <immersiveengineering:metal:35>
		//liquid: <liquid:uranium>
	},
	soulforgedSteel: {
		block: <betterwithmods:steel_block>,
		ingot: <betterwithmods:material:14>,
		nugget: <betterwithmods:material:30>,
		plate: <betterwithmods:material:51>
	}
};
