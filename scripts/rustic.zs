

recipes.remove(<rustic:condenser>);
recipes.addShaped("rustic_alchemicCondenser",<rustic:condenser>,
	[[null,<minecraft:brick>,null],
	 [<minecraft:brick>,<bloodmagic:blood_tank>,<minecraft:brick>],
	 [<minecraft:brick>,<minecraft:stained_hardened_clay>,<minecraft:brick>]]);

recipes.remove(<rustic:condenser_advanced>);
recipes.addShaped("rustic_alchemicCondenserAdvanced",<rustic:condenser_advanced>,
	[[null,<minecraft:netherbrick:1>,null],
	 [<minecraft:netherbrick:1>,<bloodmagic:blood_tank>,<minecraft:netherbrick:1>],
	 [<minecraft:netherbrick:1>,<ore:blockOsmium>,<minecraft:netherbrick:1>]]);
