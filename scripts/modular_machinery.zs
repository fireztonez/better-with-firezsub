import crafttweaker.item.IItemStack;
import crafttweaker.oredict.IOreDictEntry;


recipes.addShaped("modularmachinery_blockcasing",<modularmachinery:blockcasing>*2,
  [[<ore:plateModularium>,<ore:plateModularium>,<ore:plateModularium>],
   [<ore:plateModularium>,null,<ore:plateModularium>],
   [<ore:plateModularium>,<ore:plateModularium>,<ore:plateModularium>]]);

recipes.addShaped("modularmachinery_blockcasing_vent",<modularmachinery:blockcasing:1>*2,
  [[<ore:plateModularium>,<ore:slatIron>,<ore:plateModularium>],
   [<ore:slatIron>,null,<ore:slatIron>],
   [<ore:plateModularium>,<ore:slatIron>,<ore:plateModularium>]]);

recipes.remove(<modularmachinery:blockcasing:2>*2);
recipes.addShaped("modularmachinery_blockcasing_firebox",<modularmachinery:blockcasing:2>*2,
 [[<ore:plateRedstoneAlloy>,<ore:slatIron>,<ore:plateRedstoneAlloy>],
  [<modularmachinery:blockcasing>,<minecraft:fire_charge>,<modularmachinery:blockcasing>],
  [<ore:plateRedstoneAlloy>,<ore:slatIron>,<ore:plateRedstoneAlloy>]]);

recipes.addShaped("modularmachinery_blockcasing_gearbox",<modularmachinery:blockcasing:3>*2,
  [[<ore:plateModularium>,<ore:gearSteel>,<ore:plateModularium>],
   [<ore:gearRedstoneAlloy>,null,<ore:gearRedstoneAlloy>],
   [<ore:plateModularium>,<ore:gearSteel>,<ore:plateModularium>]]);

recipes.addShaped("modularmachinery_blockcasing_circuitry",<modularmachinery:blockcasing:5>*2,
 [[<ore:plateModularium>,<immersiveengineering:material:27>,<ore:plateModularium>],
  [<immersiveengineering:wirecoil:1>,null,<immersiveengineering:wirecoil:1>],
  [<ore:plateModularium>,<immersiveengineering:material:27>,<ore:plateModularium>]]);



recipes.addShaped("modularmachinery_smallItemOutput",<modularmachinery:blockoutputbus:2>,
 [[<ore:plateModularium>,<ore:chestWood>,<ore:plateModularium>],
  [null,<modularmachinery:blockcasing>,null],
  [<ore:plateModularium>,<ore:hopper>,<ore:plateModularium>]]);



recipes.remove(<modularmachinery:blockinputbus:1>);
recipes.addShaped("modularmachinery_itemInputBusSmall", <modularmachinery:blockinputbus:1>,
  [[<ore:plateModularium>,<ore:hopper>,<ore:plateModularium>],
   [null,<modularmachinery:blockcasing>,null],
   [<ore:plateModularium>,<ore:chestWood>,<ore:plateModularium>]]);

recipes.remove(<modularmachinery:blockfluidoutputhatch:1>);
recipes.addShaped("modularmachinery_fluidInputHatchSmall", <modularmachinery:blockfluidoutputhatch:1>,
  [[<ore:plateModularium>,<ore:hopper>,<ore:plateModularium>],
   [<minecraft:bucket>,<modularmachinery:blockfluidoutputhatch>,<minecraft:bucket>],
   [<ore:plateModularium>,null,<ore:plateModularium>]]);

recipes.remove(<modularmachinery:blockcontroller>);
recipes.addShaped("modularmachinery_blockController",<modularmachinery:blockcontroller>,
  [[<ore:plateModularium>,<immersiveengineering:wirecoil:1>,<ore:plateModularium>],
   [<ore:gearRedstoneAlloy>,<modularmachinery:blockcasing>,<ore:gearRedstoneAlloy>],
   [<ore:plateModularium>,<immersiveengineering:material:27>,<ore:plateModularium>]]);

recipes.remove(<modularmachinery:blockenergyinputhatch>);
recipes.addShaped("modularmachinery_blockEnergyInputTiny",<modularmachinery:blockenergyinputhatch>,
  [[<immersiveengineering:wirecoil>,<immersiveengineering:connector>,<immersiveengineering:wirecoil>],
   [<ore:plateRedstoneAlloy>,<modularmachinery:blockcasing>,<ore:plateRedstoneAlloy>],
   [<ore:plateModularium>,<ore:plateRedstoneAlloy>,<ore:plateModularium>]]);

recipes.remove(<modularmachinery:blockenergyinputhatch:1>);
recipes.addShaped("modularmachinery_blockEnergyInputSmall",<modularmachinery:blockenergyinputhatch:1>,
 [[<immersiveengineering:wirecoil:1>,<immersiveengineering:connector:2>,<immersiveengineering:wirecoil:1>],
  [<ore:blockRedstoneAlloy>,<modularmachinery:blockcasing>,<ore:blockRedstoneAlloy>],
  [<ore:plateModularium>,<immersiveengineering:material:9>,<ore:plateModularium>]]);
