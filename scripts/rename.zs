import crafttweaker.item.IItemStack;

<primal:hide_tanned>.displayName = "Wet Tanned Hide";
<primal:logs_stacked:0>.displayName = "Oak Wood Pile";
<primal:logs_stacked:1>.displayName = "Spruce Wood Pile";
<primal:logs_stacked:2>.displayName = "Birch Wood Pile";
<primal:logs_stacked:3>.displayName = "Jungle Wood Pile";
<primal:logs_stacked:4>.displayName = "Acacia Wood Pile";
<primal:logs_stacked:5>.displayName = "Dark Oak Pile Wood";
<primal:logs_stacked:6>.displayName = "Ironwood Wood Pile";
<primal:logs_stacked:7>.displayName = "Yew Wood Pile";
<cyclicmagic:sandstone_pickaxe>.displayName = "Copper Pickaxe";
<cyclicmagic:sandstone_axe>.displayName = "Copper Axe";
<cyclicmagic:sandstone_spade>.displayName = "Copper Shovel";
<cyclicmagic:sandstone_hoe>.displayName = "Copper Hoe";
<minecraft:quartz>.displayName = "Quartz";
