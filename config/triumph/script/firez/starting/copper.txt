//Set icon item
setIcon(<immersiveengineering:metal>)

//Set title String
setTitle("Find Copper")

//Set description String
setDescription("Find Malachite or Azurite to get copper")

//Add a parent advancement. The syntax is "modid:folder/path/to/the/advancement/then/filename"
addParent("firez:starting/stone_pick")

//Set the position
setPos(-1859,140)

//Set this to use direct lines, so I can have a pretty shape
drawDirectLines(true)

//Adds criteria named "hasStickOfTruth" with the trigger type "minecraft:inventory_changed". This function returns the criteria as an object
//criteria = addCriteria("hasCopper", "minecraft:inventory_changed")

setRequirements("any")

//Criteria Mine copper...
criteria1 = addCriteria("harvestBlock1", "triumph:player_harvest_block")
criteria1.setBlock("geolosys:ore", 2)

criteria2 = addCriteria("harvestBlock2", "triumph:player_harvest_block")
criteria2.setBlock("geolosys:ore", 3)