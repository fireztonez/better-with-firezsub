
setIcon("minecraft:stone_pickaxe")
setTitle("Stone Pick")
setDescription("Get yourself a better pickaxe to be able to get some resources")

addParent("firez:starting/wooden_pick")

alwaysVisible()

//Set the position
setPos(-1905,172)

//Set this to use direct lines, so I can have a pretty shape
drawDirectLines(true)

//Criteria
criteria = addCriteria("crafted", "minecraft:inventory_changed")
criteria.setItem("minecraft:stone_pickaxe")